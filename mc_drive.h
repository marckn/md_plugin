#ifndef __MCDRIVE__
#define __MCDRIVE__

/*
A very simple program written in C
(with C++ syntax). It does classical 
Monte Carlo to obtain samples from the Boltzmann distribution 
and returns the necessary forces to propagate the trajectories.
*/

#include<iostream>
#include<random>
#include<map>
#include<cmath>
#include<sstream>
#include <exception>

#include<armadillo>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "forcefields/oss.h"
#include "forcefields/charmm/init_ff.h"

namespace pt = boost::property_tree;

namespace plug_mcdrive
{
	int N,mpirank,dimensions;
	bool verbose;
	
	arma::mat initial_config;
	arma::mat current_config;
	
	std::vector<std::string> tags;
	std::vector<double> masses;
	std::vector<double> charges;
	
	int coupled_sub,coupled_idx;
		
	std::vector<int> freeze_list;
	
	// MC parameters
	bool useMC;
	int MCS;
	double MRT_delta;
	
	
	// PIMD SAMPLING ?
	std::vector<int> pimd_list;
	int pimd_beads;
	double spring_coeff;
	std::vector<arma::mat> pimd_momenta;
	std::vector<arma::mat> pimd_bead_coords;
	
	
	
	
	// MD parameters
	bool useMD;
	double md_dt;
	double md_gammath;
	int md_nsteps;
	int md_equil;
	
	
	
	
	
	// restraints
	struct Rest{
		int i1,i2;
		int nexp;
		double d;
		double k;
	};
	typedef struct Rest Rest;
	std::vector<Rest> restraints;	
	std::vector<Rest> sampling_restraints;	
	
	
	struct RefRest{
		int idx;
		int nexp;
		double k;
		arma::vec ref;
	};
	typedef struct RefRest RefRest;
	std::vector<RefRest> referenceRestraints;	
	
	// subsystems
	struct Subs{
		int Ntot;
		double verlet_r,cutoff_r;
		
		std::string tag;
		std::string potential_type;
		// for OSS potential type
		arma::mat oss_nO;
		arma::mat oss_nH;
		POSS p_oss;
		std::vector<int> nO_to_cfg;
		std::vector<int> nH_to_cfg;
		std::vector<int> nall_to_cfg;
		std::vector<int> nall_to_nonh;
		
		
		// for charmm forcefield
		std::string charmmdir;
		std::string topology;
		double** charmm_buff;
		double** charmm_gradients;
		std::vector<int> charmm_to_cfg;
		oorcharmm charmm_ff;
		
		// for generic LJ
		arma::mat ljcoords;
		std::vector<int> lj_to_cfg;
		std::vector<double> ljepsilon;
		std::vector<double> ljsigma;
		
		
	};
	typedef struct Subs Subs;
	
	struct ISubs{
		// always LJ type (for now)
		std::string tag1,tag2,type1,type2;
		std::vector<int> s1tocfg;
		std::vector<int> s2tocfg;
		std::vector<double> ljepsilon_s1;
		std::vector<double> ljsigma_s1;
		std::vector<double> charge_s1;
		
		std::vector<double> ljepsilon_s2;
		std::vector<double> ljsigma_s2;
		std::vector<double> charge_s2;
		
	};
	typedef struct ISubs ISubs;
	
	struct VSite{
		int i1,i2,isite;
		double a,b;
	};
	typedef struct VSite VSite;
	
	std::vector<Subs> subsystems;
	std::vector<ISubs> intersub;
	std::vector<VSite> vsites;
	
	void loadOSSSubs(pt::ptree& inp, std::vector<std::vector<double> >& cfg);
	void loadCharmmSubs(pt::ptree& inp, std::vector<std::vector<double> >& cfg);
	void loadLJSubs(pt::ptree& inp, std::vector<std::vector<double> >& cfg);
	void loadInterSub(std::string& tag1, std::string& tag2, pt::ptree& inp);
	
	std::vector<std::pair<std::string, std::vector<double> > > loadXYZfile(std::string& fname);
	std::vector<std::pair<std::string, std::vector<double> > > loadConfig(pt::ptree& inp);
	
	
	// units
	double hbar,length,time,mass,energy,kb,epsilon0,charge;
	double beta,T;
	double coulfac,econv_to_md;
	
	
		
	std::mt19937 rgen;
	std::uniform_real_distribution<double> dist;
	std::normal_distribution<double> ndist;
	
	
	extern "C" void loadData(int rank, const std::string& fname="mcinput.xml",bool _verbose=false);
	extern "C" int getSolventN();
	extern "C" int getDimensions(){return dimensions;}
	
	extern "C" arma::vec getMasses();
	extern "C" std::vector<std::string> getTags(){return tags;}
	extern "C" arma::mat Sample();	
	extern "C" arma::mat Forces(const arma::mat& config);
	extern "C" double totalEnergy(const arma::mat& config);
	extern "C" double atomEnergy(const arma::mat& config,int nsolv);

	arma::mat ForcesMD(const arma::mat& config);
	
	extern "C" std::map<std::string,double> getUnits()
	{
		std::map<std::string,double> units;
		units["hbar"]=hbar;
		units["length"]=length;
		units["time"]=time;
		units["mass"]=mass;
		units["energy"]=energy;
		units["kb"]=kb;
		units["beta"]=beta;
		units["epsilon0"] = epsilon0;
		units["charge"] = charge;
		return units;
	}
	
	
	arma::mat ossForces(Subs& s, const arma::mat& cfg);
	double ossEnergy(Subs& s, const arma::mat& cfg);
	
	arma::mat charmmForces(Subs& s, const arma::mat& cfg);
	double charmmEnergy(Subs& s, const arma::mat& cfg);
	
	
	arma::mat intersubForces(ISubs& s, const arma::mat& cfg);
	double intersubEnergy(ISubs& s, const arma::mat& cfg);
	
	arma::mat restForces(Rest& r, const arma::mat& cfg);
	arma::mat refRestForces(RefRest& r, const arma::mat& cfg);
	
	double restEnergy(Rest& r, const arma::mat& cfg);
	double refRestEnergy(RefRest& r, const arma::mat& cfg);
	
	
	// sampling functions
	arma::mat MDSample(int nsteps=md_nsteps);
	arma::mat MCSample();
	
	double kineticEnergy(arma::mat& cfg);
	void vrescale(arma::mat& cfg);
	void langevin(arma::mat& cfg, double dt);
	
	
	void restart(std::string dirpath);
	void equilibrate();
	
	std::ofstream tmpo;
	std::ofstream tmpe;
	void gogogo();
	
}

#endif
