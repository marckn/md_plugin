#ifndef __INITFF__
#define __INITFF__
#include<string>
#include<iostream>
#include<sstream>
#include<fstream>
#include<cmath>
#include "itp_parser.h"

struct Bonds
{
	int nbonds;
	int* ai;
	int* aj;
	
	int* hmany;
	
	double** b0;
	double** kb;
};

struct UreyBradley
{
	int nub;
	int* ai;
	int* aj;
	int* ak;
	int* hmany;
	
	double** rub;
	double** kub;
};

struct Angles
{
	int nangles;
	int* ai;
	int* aj;
	int* ak;
	
	int* hmany;
	
	double** kth;
	double** th0;
};

struct Dihedrals
{
	int ndih;
	int* ai;
	int* aj;
	int* ak;
	int* al;
	
	int* hmany;
	
	double** delta;
	double** kchi;
	int** mult;
};

struct Pairs
{
	int npa;
	int* ai;
	int* aj;
	
	double* eps;
	double* sigma;
};

struct Nonbonded
{
	std::vector<int> verletidx;
	int nnb;
	int* ai;
	int* aj;
};
struct Atomtable
{
	int natoms;
	std::string* atomtype;
	std::string* atomname;
	double* atomcharge;
	double* atommass;
	double* epsilon;
	double* sigma;
	
};

struct oorcharmm
{
		
		std::vector<std::vector<std::vector<double> > > dist_table;
		double Epot;
		bool epot_need_update;
		double histch;
		
		double** coords_in;
		double** old_stored;
		double verlet_r, cutoff_r;
		
		void init(double _L[] ,bool _pbc,std::string& _topo, double _verlet=2, double _cutoff=2, double _econv=1, double _fconv=1, bool _silent=true, std::string _ff_basedir="./")
		{
			ff_basedir = _ff_basedir;
			silent=_silent;
			econv=_econv;
			fconv=_fconv;
			topo_file=_topo;
			if(!silent)
				std::cout<<"oorcharmm(): Initializing"<<std::endl;
			
			readAtomTable();
			initBonds();
			initAngles();
			initDihedrals();
			initPairs();
			initNonbonded();
	
			L[0]=_L[0];
			L[1]=_L[1];
			L[2]=_L[2];
			PIg=acos(-1.);
	
	
			pbc=_pbc;
			old_stored = new double*[Ntot];
			histch=0;
			for(int i=0;i<Ntot;i++)
			{
				std::vector<double> zvl(4,0.0);
				std::vector<std::vector<double> > vrow(i,zvl);
				dist_table.push_back(vrow);
				old_stored[i]= new double[3];
				old_stored[i][0]=old_stored[i][1]=old_stored[i][2]=0;
			}
			
			epot_need_update=true;
			Epot=0;
			verlet_r = _verlet;
			cutoff_r = _cutoff;
	
	

		}
		
		void feedCoordinates(double** _coords_in, bool updateVerlet=true)
		{
			coords_in = _coords_in;
			// update dist table
			bool haschanged=false;
			updateVerlet=false;
			double dcmax = 0;
			for(int i=0;i<Ntot;i++)
				for(int d=0;d<3;d++)
				{	
					double dcng = fabs(coords_in[i][d]-old_stored[i][d]); 
					if(dcng > 1E-12)
					{
						haschanged=true;
						
						dcmax = std::max(dcmax,dcng);
						if(histch+dcng > (verlet_r - cutoff_r))
						{
							updateVerlet=true;
							break;
						}
					}
				}
			
			histch += dcmax;
			if(!haschanged)
				return;
			
			for(int i=0;i<Ntot;i++)
			{
				for(int d=0;d<3;d++)
					old_stored[i][d] = _coords_in[i][d];
					
				for(int j=0;j<i;j++)
				{
					double dst[4];
					fdist(_coords_in[i],_coords_in[j],dst);
					for(int k=0;k<4;k++)
						dist_table[i][j][k]=dst[k];
					
				}
			}
			
			
			
			// update verlet table
			if(updateVerlet)
			{
			
				histch=0;
				atomnb.verletidx.clear();
				for(int i=0;i<atomnb.nnb;i++)
				{
					double dst[4];
					idist(atomnb.ai[i],atomnb.aj[i],dst);
					if(dst[3]<verlet_r)
						atomnb.verletidx.push_back(i);
				
				}
			}
			
			epot_need_update=true;
		}
		
		double potentialEnergy()
		{
			if(!epot_need_update)
				return Epot;
			
			epot_need_update=false;
			double eptot = 0;
			double en=0;
			en = bondsEnergy();
			//std::cout<<"BONDS "<<en<<std::endl;
			eptot += en;
			
			if(std::isnan(eptot))
			{
				std::cout<<"BONDS NAN"<<std::endl;
				eptot=0;	
			}
			en = ureybradleyEnergy();
			eptot+=en;
			//std::cout<<"U-B "<<en<<std::endl;
			
			
			if(std::isnan(eptot))
			{
				std::cout<<"UREYBRADLEY NAN"<<std::endl;
				eptot=0;	
			}
			
			en = anglesEnergy();
			eptot += en;
			//std::cout<<"ANGLES "<<en<<std::endl;
			
			
			if(std::isnan(eptot))
			{
				std::cout<<"ANGLES NAN"<<std::endl;
				eptot=0;	
			}
			
			en = dihedralsEnergy();
			eptot += en;
			//std::cout<<"DIHEDRALS "<<en<<std::endl;
			
			if(std::isnan(eptot))
			{
				std::cout<<"DIHEDRALS NAN"<<std::endl;
				eptot=0;	
			}
			
			en = pairsEnergy();
			eptot += en;
			//std::cout<<"PAIRS "<<en<<std::endl;
			
			if(std::isnan(eptot))
			{
				std::cout<<"PAIRS NAN"<<std::endl;
				eptot=0;	
			}
			
			en = nonbondEnergy();
			eptot+= en;
			//std::cout<<"NONBONDED "<<en<<std::endl;
			
			if(std::isnan(eptot))
			{
				std::cout<<"NONBONDED NAN"<<std::endl;
				eptot=0;	
			}
			//std::cout<<"TOTAL "<<eptot<<std::endl;
			Epot = eptot;
			return eptot;
		}

		void gradient(double** grad)
		{
			Epot=0;
			epot_need_update=false;
			
			for(int i=0;i<Ntot;i++)
				grad[i][0]=grad[i][1]=grad[i][2]=0;
	
			Epot += bondsGradient(grad);
			Epot += ureybradleyGradient(grad);
			Epot += anglesGradient(grad);
			Epot += dihedralsGradient(grad);
			Epot += pairsGradient(grad);
			Epot += nonbondGradient(grad);
	
		}

		Atomtable atomtable;
		int totalAtoms(){return Ntot;}
		
		void readAtomTable()
		{
			itp_parser par;
			par.init(topo_file.c_str(),silent);
			std::string fdir = ff_basedir+"/ffnonbonded.itp";
			itp_parser datapar;
			datapar.init(fdir.c_str(),silent);
			
			itp_element atoms;
			itp_element atomdata;
			
			par.getElement("[ atoms ]",8,atoms);
			datapar.getElement("[ atomtypes ]",7,atomdata);
	
			atomtable.natoms = atoms.nout;
			atomtable.atomtype = new std::string[atomtable.natoms];
			atomtable.atomname = new std::string[atomtable.natoms];
			atomtable.atomcharge = new double[atomtable.natoms];
			atomtable.atommass = new double[atomtable.natoms];
			atomtable.epsilon = new double[atomtable.natoms];
			atomtable.sigma = new double[atomtable.natoms];
	
			for(int i=0;i<atomtable.natoms;i++)
			{
				std::stringstream vals;
				vals<<atoms.elements[i][1]<<" "<<atoms.elements[i][4]<<" "<<atoms.elements[i][6]<<" "<<atoms.elements[i][7];
				vals>>atomtable.atomtype[i]>>atomtable.atomname[i]>>atomtable.atomcharge[i]>>atomtable.atommass[i];
		
				std::stringstream query;
				query<<atomtable.atomtype[i]<<" //";
				itp_element result;
				queryElement(atomdata,result,query);
				if(result.nout>1 && !silent)
					std::cout<<"readAtomTable(): WARNING. Multiple entries in atomtypes for type "<<atomtable.atomtype[i]<<std::endl;
		
				std::stringstream val;
				val<<result.elements[0][5]<<" "<<result.elements[0][6];
				val>>atomtable.sigma[i]>>atomtable.epsilon[i];
				free_itp_element(result);
		
			}
	
			free_itp_element(atoms);
			free_itp_element(atomdata);
			Ntot = atomtable.natoms;
			if(!silent)
				std::cout<<"readAtomTable(): read "<<atomtable.natoms<<" atoms"<<std::endl;
		
		}
		
		void initBonds()
		{
			itp_parser atmpar;
			atmpar.init(topo_file.c_str(),silent);
			std::string fdir = ff_basedir+"/ffbonded.itp";
			itp_parser datapar;
			datapar.init(fdir.c_str(),silent);
	
			itp_element elebond;
			itp_element bonddata;
	
			atmpar.getElement("[ bonds ]",2,elebond);
			datapar.getElement("[ bondtypes ]",5,bonddata);
	
			atombonds.nbonds = elebond.nout;
			atombonds.ai = new int[atombonds.nbonds];
			atombonds.aj = new int[atombonds.nbonds];
			atombonds.hmany = new int[atombonds.nbonds];
	
			atombonds.b0 = new double*[atombonds.nbonds];
			atombonds.kb = new double*[atombonds.nbonds];
			int ntot=0;
			for(int i=0;i<atombonds.nbonds;i++)
			{
				std::stringstream eleval;
				eleval<<elebond.elements[i][0]<<" "<<elebond.elements[i][1];
				eleval>>atombonds.ai[i]>>atombonds.aj[i];
				atombonds.ai[i]--;
				atombonds.aj[i]--;
				std::string type_i = atomtable.atomtype[atombonds.ai[i]];
				std::string type_j = atomtable.atomtype[atombonds.aj[i]];
				
				std::stringstream query;
				query<<type_i<<" "<<type_j<<" // "<<type_j<<" "<<type_i<<" //";
				itp_element result;
				queryElement(bonddata,result,query);
				atombonds.hmany[i]=result.nout;
				if(result.nout==0 && !silent)
					std::cout<<"initBonds(): cannot find bond data between "<<type_i<<" and "<<type_j<<std::endl;
		
				atombonds.b0[i] = new double[result.nout];
				atombonds.kb[i] = new double[result.nout];
				for(int j=0;j<result.nout;j++)
				{
					std::stringstream vals;
					vals<<result.elements[j][3]<<" "<<result.elements[j][4];
					vals>>atombonds.b0[i][j]>>atombonds.kb[i][j];
				}
				ntot += result.nout;
				free_itp_element(result);
			}
			if(!silent)
				std::cout<<"initBonds(): read "<<atombonds.nbonds<<" bonds consisting of "<<ntot<<" sum terms"<<std::endl;
	
			free_itp_element(elebond);
			free_itp_element(bonddata);
		}
		
		
		void initAngles()
		{
			itp_parser atmpar;
			atmpar.init(topo_file.c_str(),silent);
			std::string fdir = ff_basedir+"/ffbonded.itp";
			itp_parser datapar;
			datapar.init(fdir.c_str(),silent);
	
			itp_element eleang;
			itp_element angdata;
		
			atmpar.getElement("[ angles ]",3,eleang);
			datapar.getElement("[ angletypes ]",8,angdata);
	
			atomangles.nangles = eleang.nout;
			atomangles.ai = new int[atomangles.nangles];
			atomangles.aj = new int[atomangles.nangles];
			atomangles.ak = new int[atomangles.nangles];
	
			atomangles.hmany = new int[atomangles.nangles];
	
			atomangles.kth = new double*[atomangles.nangles];
			atomangles.th0 = new double*[atomangles.nangles];
	
			int ntot=0;
			int ubcounter=0;
	
			for(int i=0;i<atomangles.nangles;i++)
			{
				std::stringstream eleval;
				eleval<<eleang.elements[i][0]<<" "<<eleang.elements[i][1]<<" "<<eleang.elements[i][2];
				eleval>>atomangles.ai[i]>>atomangles.aj[i]>>atomangles.ak[i];
				atomangles.ai[i]--;
				atomangles.aj[i]--;
				atomangles.ak[i]--;
		
				std::string type_i = atomtable.atomtype[atomangles.ai[i]];
				std::string type_j = atomtable.atomtype[atomangles.aj[i]];
				std::string type_k = atomtable.atomtype[atomangles.ak[i]];
				
				std::stringstream query;
				query<<type_i<<" "<<type_j<<" "<<type_k<<" // "<<type_k<<" "<<type_j<<" "<<type_i<<" //";
				itp_element result;
				queryElement(angdata,result,query);
				atomangles.hmany[i]=result.nout;
				if(result.nout==0 && !silent)
					std::cout<<"initAngles(): cannot find angle data between "<<type_i<<" , "<<type_j<<" and "<<type_k<<std::endl;
		
				atomangles.kth[i] = new double[result.nout];
				atomangles.th0[i] = new double[result.nout];
				bool atleastone=false;
				for(int j=0;j<result.nout;j++)
				{
					double ub0,kub;
					std::stringstream vals;
					vals<<result.elements[j][4]<<" "<<result.elements[j][5]<<" "<<result.elements[j][6]<<" "<<result.elements[j][7];
					vals>>atomangles.th0[i][j]>>atomangles.kth[i][j]>>ub0>>kub;
					if(ub0>0 && kub>0)
						atleastone=true;
				}
				if(atleastone)
					ubcounter++;
				ntot += result.nout;
		
				free_itp_element(result);
			}
	
	
			// Urey Bradley part
			atomub.nub = ubcounter;
			atomub.ai = new int[atomub.nub];
			atomub.aj = new int[atomub.nub];
			atomub.ak = new int[atomub.nub];
			atomub.hmany = new int[atomub.nub];
			atomub.rub = new double*[atomub.nub];
			atomub.kub = new double*[atomub.nub];
	
			ubcounter=0;
			int hmnub=0;
			for(int i=0;i<atomangles.nangles;i++)
			{
				std::stringstream eleval;
				eleval<<eleang.elements[i][0]<<" "<<eleang.elements[i][1]<<" "<<eleang.elements[i][2];
		
				int ai,aj,ak;
		
				eleval>>ai>>aj>>ak;
				ai--;
				aj--;
				ak--;
		
				std::string type_i = atomtable.atomtype[ai];
				std::string type_j = atomtable.atomtype[aj];
				std::string type_k = atomtable.atomtype[ak];
				
				std::stringstream query;
				query<<type_i<<" "<<type_j<<" "<<type_k<<" // "<<type_k<<" "<<type_j<<" "<<type_i<<" //";
				itp_element result;
				queryElement(angdata,result,query);
		
				int hm=0;
				for(int j=0;j<result.nout;j++)
				{
					double ub0,kub;
					std::stringstream vals;
					vals<<result.elements[j][4]<<" "<<result.elements[j][5]<<" "<<result.elements[j][6]<<" "<<result.elements[j][7];
					vals>>atomangles.th0[i][j]>>atomangles.kth[i][j]>>ub0>>kub;
					if(ub0>0 && kub>0)
						hm++;
				}
				if(hm>0)
				{
					atomub.hmany[ubcounter]=hm;
					atomub.rub[ubcounter]= new double[hm];
					atomub.kub[ubcounter]= new double[hm];
					atomub.ai[ubcounter]=ai;
					atomub.aj[ubcounter]=aj;
					atomub.ak[ubcounter]=ak;
					int idxhm=0;
					for(int j=0;j<result.nout;j++)
					{
						double ub0,kub;
						std::stringstream vals;
						vals<<result.elements[j][6]<<" "<<result.elements[j][7];
						vals>>ub0>>kub;
						if(ub0>0 && kub>0)
						{
							atomub.rub[ubcounter][idxhm]=ub0;
							atomub.kub[ubcounter][idxhm]=kub;
							idxhm++;
						}
					}
			
					ubcounter++;
				}
				hmnub += hm;
		
		
		
				free_itp_element(result);
			}
	
			if(!silent){
				std::cout<<"initAngles(): read "<<atomangles.nangles<<" angles consisting of "<<ntot<<" sum terms"<<std::endl;
				std::cout<<"initAngles(): Of the read angles "<<atomub.nub<<" have Urey-Bradley corrections for a total of "<<hmnub<<" terms"<<std::endl;
			}
			free_itp_element(eleang);
			free_itp_element(angdata);
		

		}
		
		void initDihedrals()
		{
			itp_parser atmpar;
			atmpar.init(topo_file.c_str(),silent);
			std::string fdir = ff_basedir+"/ffbonded.itp";
			itp_parser datapar;
			datapar.init(fdir.c_str(),silent);
	
			itp_element eledih;
			itp_element dihdata;
	
			atmpar.getElement("[ dihedrals ]",5,eledih);
			datapar.getElement("[ dihedraltypes ]",8,dihdata);
	
			atomdih.ndih = eledih.nout;
			atomdih.ai = new int[atomdih.ndih];
			atomdih.aj = new int[atomdih.ndih];
			atomdih.ak = new int[atomdih.ndih];
			atomdih.al = new int[atomdih.ndih];
	
			atomdih.hmany = new int[atomdih.ndih];
	
			atomdih.kchi = new double*[atomdih.ndih];
			atomdih.delta = new double*[atomdih.ndih];
			atomdih.mult = new int*[atomdih.ndih];
	
	
	
	
			int ntot=0;
			for(int i=0;i<atomdih.ndih;i++)
			{
				std::stringstream eleval;
				std::string func;
		
				eleval<<eledih.elements[i][0]<<" "<<eledih.elements[i][1]<<" "<<eledih.elements[i][2]<<" "<<eledih.elements[i][3]<<" "<<eledih.elements[i][4];
				eleval>>atomdih.ai[i]>>atomdih.aj[i]>>atomdih.ak[i]>>atomdih.al[i]>>func;
				atomdih.ai[i]--;
				atomdih.aj[i]--;
				atomdih.ak[i]--;
				atomdih.al[i]--;
		
		
				std::string type_i = atomtable.atomtype[atomdih.ai[i]];
				std::string type_j = atomtable.atomtype[atomdih.aj[i]];
				std::string type_k = atomtable.atomtype[atomdih.ak[i]];
				std::string type_l = atomtable.atomtype[atomdih.al[i]];
		
		
		
				std::stringstream query1;
				query1<<type_i<<" "<<type_j<<" "<<type_k<<" "<<type_l<<" "<<func<<" //";
				itp_element result;
				queryElement(dihdata,result,query1);
		
				if(result.nout==0)
				{
					std::stringstream query2;
					query2<<type_i<<" "<<type_k<<" "<<type_j<<" "<<type_l<<" "<<func<<" //";
					queryElement(dihdata,result,query2);
				}
		
				if(result.nout==0)
				{
					std::stringstream query3;
					query3<<type_l<<" "<<type_k<<" "<<type_j<<" "<<type_i<<" "<<func<<" //";
					queryElement(dihdata,result,query3);
				}
		
				if(result.nout==0)
				{
					std::stringstream query4;
					query4<<type_l<<" "<<type_j<<" "<<type_k<<" "<<type_i<<" "<<func<<" //";
					queryElement(dihdata,result,query4);
				}
		
				if(result.nout==0)
				{
					std::stringstream query5;
					query5<<type_i<<" "<<type_j<<" "<<type_k<<" X "<<func<<" //";
					queryElement(dihdata,result,query5);
				}
		
				if(result.nout==0)
				{
					std::stringstream query6;
					query6<<type_l<<" "<<type_j<<" "<<type_k<<" X "<<func<<" //";
					queryElement(dihdata,result,query6);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_i<<" "<<type_k<<" "<<type_j<<" X "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_l<<" "<<type_k<<" "<<type_j<<" X "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<"X "<<type_j<<" "<<type_k<<" "<<type_l<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<"X "<<type_j<<" "<<type_k<<" "<<type_i<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<"X "<<type_k<<" "<<type_j<<" "<<type_l<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<"X "<<type_k<<" "<<type_j<<" "<<type_i<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_i<<" X "<<type_k<<" "<<type_l<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_i<<" X "<<type_j<<" "<<type_l<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_l<<" X "<<type_k<<" "<<type_i<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_l<<" X "<<type_j<<" "<<type_i<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_i<<" "<<type_j<<" X "<<type_l<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_i<<" "<<type_k<<" X "<<type_l<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_l<<" "<<type_j<<" X "<<type_i<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_l<<" "<<type_k<<" X "<<type_i<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
		
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<"X "<<type_j<<" "<<type_k<<" X "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<"X "<<type_k<<" "<<type_j<<" X "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_i<<" X X "<<type_l<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				if(result.nout==0)
				{
					std::stringstream query2palle;
					query2palle<<type_l<<" X X "<<type_i<<" "<<func<<" //";
					queryElement(dihdata,result,query2palle);
				}
		
				atomdih.hmany[i]=result.nout;
		
				if(result.nout==0 && !silent)
					std::cout<<"initDihedrals(): cannot find dihedral data between "<<type_i<<" , "<<type_j<<" , "<<type_k<<" and "<<type_l<<" and func = "<<func<<std::endl;
		
				atomdih.delta[i] = new double[result.nout];
				atomdih.kchi[i] = new double[result.nout];
				atomdih.mult[i]=new int[result.nout];
		
				for(int j=0;j<result.nout;j++)
				{
					std::stringstream vals;
					vals<<result.elements[j][4]<<" "<<result.elements[j][5]<<" "<<result.elements[j][6]<<" "<<result.elements[j][7];
					int func;
					vals>>func;
			
					vals>>atomdih.delta[i][j]>>atomdih.kchi[i][j];
			
					if(func==9)
						vals>>atomdih.mult[i][j];
					else
						atomdih.mult[i][j]=0;
				}
				ntot += result.nout;
		
				free_itp_element(result);
			}
			if(!silent)
				std::cout<<"initDihedrals(): read "<<atomdih.ndih<<" Dihedrals consisting of "<<ntot<<" sum terms"<<std::endl;
	
			free_itp_element(eledih);
			free_itp_element(dihdata);
		}
		
		void initPairs()
		{
			itp_parser atmpar;
			atmpar.init(topo_file.c_str(),silent);
			std::string fdir = ff_basedir+"/ffnonbonded.itp";
			itp_parser datapar;
			datapar.init(fdir.c_str(),silent);
			itp_element elepair,pairdata;
			atmpar.getElement("[ pairs ]",2,elepair);
			datapar.getElement("[ pairtypes ]",5,pairdata);
	
			atompairs.npa = elepair.nout;
			atompairs.ai = new int[atompairs.npa];
			atompairs.aj = new int[atompairs.npa];
	
			atompairs.eps = new double[atompairs.npa];
			atompairs.sigma = new double[atompairs.npa];
	
			for(int i=0;i<atompairs.npa;i++)
			{
				std::stringstream eleval;
				eleval<<elepair.elements[i][0]<<" "<<elepair.elements[i][1];
				eleval>>atompairs.ai[i]>>atompairs.aj[i];
				atompairs.ai[i]--;
				atompairs.aj[i]--;
		
				std::string type_i = atomtable.atomtype[atompairs.ai[i]];
				std::string type_j = atomtable.atomtype[atompairs.aj[i]];
				std::stringstream query;
				query<<type_i<<" "<<type_j<<" // "<<type_j<<" "<<type_i<<" //";
				itp_element result;
				queryElement(pairdata,result,query);
		
				if(result.nout==0)
				{
					atompairs.eps[i] = sqrt(atomtable.epsilon[atompairs.ai[i]]*atomtable.epsilon[atompairs.aj[i]]);
					atompairs.sigma[i] = 0.5*(atomtable.sigma[atompairs.ai[i]]+atomtable.sigma[atompairs.aj[i]]);
				}
				else
				{
					std::stringstream vals;
					vals<<result.elements[0][3]<<" "<<result.elements[0][4];
					vals>>atompairs.sigma[i]>>atompairs.eps[i];
				}
			
			}
	
			if(!silent)
				std::cout<<"initPairs(): read "<<atompairs.npa<<" pairs."<<std::endl;
	
			free_itp_element(elepair);

		}
		
		
		void initNonbonded()
		{
			// S-R LJ and Coulomb terms

			std::vector<std::pair<int,int> > remlist;
			
			// first the fast ones
			
			for(int i=0;i<Ntot;i++)
				for(int j=0;j<i;j++)
				{
					//1-2 bonds
					bool notthis=false;
					
					for(int k=0;k<atombonds.nbonds;k++)
						if((atombonds.ai[k]==i && atombonds.aj[k]==j)||(atombonds.ai[k]==j && atombonds.aj[k]==i))
						{
							notthis=true;
							break;
						}
					if(notthis)
						continue;
				
					for(int k=0;k<atompairs.npa;k++)
						if((i==atompairs.ai[k] && j == atompairs.aj[k]) || (j==atompairs.ai[k] && i == atompairs.aj[k]))
						{
							notthis=true;
							break;
						}
			
					if(notthis)
						continue;
			
					for(int k=0;k<atomangles.nangles;k++)
						//if( (i==atomangles.ai[k] || i == atomangles.aj[k] || i == atomangles.ak[k]) && 
						//    (j==atomangles.ai[k] || j == atomangles.aj[k] || j == atomangles.ak[k]))
						if((i==atomangles.ai[k] && j == atomangles.ak[k]) || (j==atomangles.ai[k] && i == atomangles.ak[k]) || 
						  (i == atomangles.aj[k] && (j==atomangles.ai[k] || j==atomangles.ak[k])) ||  
						  (j == atomangles.aj[k] && (i==atomangles.ai[k] || i==atomangles.ak[k])))
						
						{
							notthis=true;
							break;
						}
					
						
					if(notthis)
						continue;
						
					
					for(int k=0;k<atomdih.ndih;k++)
					{
						int ai=atomdih.ai[k];
						int aj=atomdih.aj[k];
						int ak=atomdih.ak[k];
						int al=atomdih.al[k];
						//if( (i==ai || i == aj || i == ak || i == al) &&
						//    (j==ai || j == aj || j == ak || j == al) )
						if((i==ai && (j == aj || j == ak))                 ||
						   (i==aj && (j == ai || j == ak || j == al))      ||
						   (i==al && (j == ai || j == aj || j == ak))      ||
						   (i==al && (j == ai || j == aj)) )
						{
							notthis=true;
							break;
						}  
					}
					
					if(!notthis)
						remlist.push_back(std::pair<int,int>(i,j));
					
				}		
			
			
			// connected bonds
			std::vector<std::pair<int,int> > adjbonds;
			for(int k=0;k<atombonds.nbonds;k++)
			{	
				int aik = atombonds.ai[k];
				int ajk = atombonds.aj[k];
				
				for(int l=0;l<k;l++)
				{
					int ail = atombonds.ai[l];
					int ajl = atombonds.aj[l];
					if(aik == ail || aik == ajl || ajk == ail || ajk == ajl)
					{
						adjbonds.push_back(std::pair<int,int>(k,l));
						break;
					}	
				}
			}
			
			std::vector<std::pair<int,int> > remlist2;
			
			// 1-3 inferred from bonds
			for(auto nyet : remlist)
			{
				int i = nyet.first;
				int j = nyet.second;
				bool notthis=false;
				for(auto bcoup : adjbonds)
				{
					int a1i = atombonds.ai[bcoup.first];
					int a1j = atombonds.aj[bcoup.first];
					int a2i = atombonds.ai[bcoup.second];
					int a2j = atombonds.aj[bcoup.second];
					
					// 1-2 have already been filtered out
					if( (i == a1i || i == a1j || i == a2i || i == a2j) &&
					    (j == a1i || j == a1j || j == a2i || j == a2j) )
					{
						notthis=true;
						break;
					}	
					
					
				}
				if(!notthis)
					remlist2.push_back(nyet);
			}
			
			// 1-4 inferred from bonds
			std::vector<std::pair<int,int> > remlist3;
			std::vector<std::pair<int,int> > reladjbonds;
			
			for(unsigned int k = 0; k < adjbonds.size();k++)
			{
				auto b1 = adjbonds[k];
					
				for(unsigned int l = 0; l < k ; l++)
				{
					auto b2 = adjbonds[l];
						
					if(b1.first != b2.first &&
					   b1.first != b2.second && 
					   b1.second != b2.first && 
					   b1.second != b2.second)
						continue;
					
					reladjbonds.push_back(std::pair<int,int>(k,l));	
						
				}
			}
			
			for(auto nyet : remlist2)
			{
				int i = nyet.first;
				int j = nyet.second;
				
				bool notthis=false;
				
				for(auto b : reladjbonds)
				{
					// find points at 1-4
					auto bc1 = adjbonds[b.first];
					auto bc2 = adjbonds[b.second];
					
					int b1,b2;
					if(bc1.first == bc2.first)
					{
						b1 = bc1.second;
						b2 = bc2.second;
					} else if(bc1.second == bc2.second)
					{
						b1 = bc1.first;
						b2 = bc2.first;
					} else if(bc1.second == bc2. first)
					{
						b1 = bc1.first;
						b2 = bc2.second;
					}else{
						b1 = bc1.second;
						b2 = bc2.first;
					}
					
					int a1 = atombonds.ai[b1];
					int a2 = atombonds.aj[b1];
					int a3 = atombonds.ai[b2];
					int a4 = atombonds.aj[b2];
					
					if( (i == a1 || i == a2 || i == a3 || i == a4) &&
					    (j == a1 || j == a2 || j == a3 || j == a4) )
					{
						notthis=true;
						break;
					}
				}
				
				if(!notthis)
					remlist3.push_back(nyet);
				
			}
			
			
			atomnb.nnb = remlist3.size();
			atomnb.ai = new int[atomnb.nnb];
			atomnb.aj = new int[atomnb.nnb];
			int knt=0;
			for(auto p : remlist3)
			{
				atomnb.ai[knt] = p.first;
				atomnb.aj[knt++] = p.second;
			}
			
			
			if(!silent)
				std::cout<<"initNonbonded(): Non-bonded coulomb and LJ interaction for "<<atomnb.nnb<<" couples"<<std::endl;

		}
		
		void queryElement(itp_element& src, itp_element& outp, std::string* query)
		{
			int nres=0;
			for(int i=0;i<src.nout;i++)
			{
				bool isit=true;
				for(int j=0;j<src.ncols;j++)
				{
				
					if(query[j].length()==0)
						continue;
			
					if(query[j]!=src.elements[i][j])
					{
						isit=false;
						break;
					}
				}
		
		
				if(isit)
					nres++;
			}
			outp.nout=nres;
			outp.ncols=src.ncols;
	
			outp.elements = new std::string*[outp.nout];
			nres=0;
			for(int i=0;i<src.nout;i++)
			{
				bool isit=true;
				for(int j=0;j<src.ncols;j++)
				{
			
			
					if(query[j].length()==0)
						continue;
			
					if(query[j]!=src.elements[i][j])
					{
						isit=false;
						break;
					}
			
				}
		
				if(isit)
				{
					outp.elements[nres] = new std::string[outp.ncols];
					for(int j=0;j<src.ncols;j++)
						outp.elements[nres][j]=src.elements[i][j];
					nres++;
				}
			}
		}
		
		
		void queryElement(itp_element& src, itp_element& outp, std::stringstream &query)  // OR compounded query
		{
			int nres=0;
	
			for(int i=0;i<src.nout;i++)
			{
				bool isit=false;
				while(!query.eof())
				{
					bool atleastone=true;
					for(int j=0;j<src.ncols;j++)
					{
						std::string val;
						query>>val;
						
						if(val=="-")
							continue;
						if(val=="//")
							break;
			
						if(val!=src.elements[i][j] && src.elements[i][j]!="*")
							atleastone=false;
					
				
					}
					isit = isit || atleastone;
				}
				query.seekg(0,std::ios::beg);	
				query.clear();	
				if(isit)
					nres++;
			}
			query.seekg(0,std::ios::beg);
			query.clear();
			outp.nout=nres;
			outp.ncols=src.ncols;
	
			outp.elements = new std::string*[outp.nout];
			nres=0;
			for(int i=0;i<src.nout;i++)
			{
				bool isit=false;
				while(!query.eof())
				{
					bool atleastone=true;
					for(int j=0;j<src.ncols;j++)
					{
			
						std::string val;
						query>>val;
						
						if(val=="-")
							continue;
						if(val=="//")
							break;
			
						if(val!=src.elements[i][j] && src.elements[i][j]!="*")
							atleastone=false;
				
				
					
					}
					isit = isit || atleastone;
				}
		
				query.seekg(0,std::ios::beg);	
				query.clear();
				if(isit)
				{
					outp.elements[nres] = new std::string[outp.ncols];
					for(int j=0;j<src.ncols;j++)
						outp.elements[nres][j]=src.elements[i][j];
					nres++;
				}
			}
		}
		
		Bonds atombonds;
		UreyBradley atomub;
		Angles atomangles;
		Dihedrals atomdih;
		Pairs atompairs;
		Nonbonded atomnb;
		
		
		int Ntot;
		double PIg;
		double L[3];
		bool pbc;
		double bondsEnergy()
		{
			double ptot=0;
			for(int i=0;i<atombonds.nbonds;i++)
			{
				double dist[4];
				//fdist(coords_in[atombonds.ai[i]],coords_in[atombonds.aj[i]],dist);
				idist(atombonds.ai[i],atombonds.aj[i],dist);
		
				for(int j=0;j<atombonds.hmany[i];j++)
				{
					double eterm = 0.5*atombonds.kb[i][j]*pow(dist[3]-atombonds.b0[i][j],2)*econv;
					ptot += eterm;
				}
			}
			return ptot;
	
		}
		
		

		double ureybradleyEnergy()
		{
			double ptot=0;
			for(int i=0;i<atomub.nub;i++)
			{
				double dist[4];
				//fdist(coords_in[atomub.ai[i]],coords_in[atomub.ak[i]],dist);
				idist(atomub.ai[i],atomub.ak[i],dist);
				
				for(int j=0;j<atomub.hmany[i];j++)
				{
					double eterm = 0.5*atomub.kub[i][j]*pow(dist[3]-atomub.rub[i][j],2)*econv;
					ptot += eterm;
				}
			}
			return ptot;
	
		}
		

		double anglesEnergy()
		{
			double ptot=0;
			for(int i=0;i<atomangles.nangles;i++)
			{
				double veca[4];
				double vecb[4];
		
				fdist(coords_in[atomangles.ai[i]],coords_in[atomangles.aj[i]],veca);
				fdist(coords_in[atomangles.ak[i]],coords_in[atomangles.aj[i]],vecb);
				
				//idist(atomangles.ai[i],atomangles.aj[i],veca);
				//idist(atomangles.ak[i],atomangles.aj[i],vecb);
			
			
				double targ = (veca[0]*vecb[0]+veca[1]*vecb[1]+veca[2]*vecb[2])/(veca[3]*vecb[3]);
				
				if(targ>1) targ=1;
				if(targ<-1) targ=-1;
				
				
				double	theta = acos(targ)*180./PIg;
		
				
				for(int j=0;j<atomangles.hmany[i];j++)
				{
					double eterm = 0.5*atomangles.kth[i][j]*pow((theta-atomangles.th0[i][j])*PIg/180,2)*econv;
					ptot += eterm;
				}
				
			}
	
			return ptot;
		}

		double dihedralsEnergy()
		{
	
			double ptot=0;
			for(int i=0;i<atomdih.ndih;i++)
			{
				double vecij[4];
				double vecjk[4];
				double veckl[4];
		
		
				//fdist(coords_in[atomdih.aj[i]],coords_in[atomdih.ai[i]],vecij);
				//fdist(coords_in[atomdih.ak[i]],coords_in[atomdih.aj[i]],vecjk);
				//fdist(coords_in[atomdih.al[i]],coords_in[atomdih.ak[i]],veckl);
				idist(atomdih.aj[i],atomdih.ai[i],vecij);
				idist(atomdih.ak[i],atomdih.aj[i],vecjk);
				idist(atomdih.al[i],atomdih.ak[i],veckl);
		
				
		
				double v1[4]={vecij[1]*vecjk[2]-vecij[2]*vecjk[1],vecij[2]*vecjk[0]-vecij[0]*vecjk[2],vecij[0]*vecjk[1]-vecij[1]*vecjk[0],0};
				double v2[4]={vecjk[1]*veckl[2]-vecjk[2]*veckl[1],vecjk[2]*veckl[0]-vecjk[0]*veckl[2],vecjk[0]*veckl[1]-vecjk[1]*veckl[0],0};
		
				double cdot=0;
				for(int ll=0;ll<3;ll++)
				{
					v1[3]+= v1[ll]*v1[ll];
					v2[3]+= v2[ll]*v2[ll];
					cdot += v1[ll]*v2[ll];
				}
				v1[3]=sqrt(v1[3]);
				v2[3]=sqrt(v2[3]);
		
				if(v1[3]<1E-10 || v2[3]<1E-10)
					continue;
				
				double targ = cdot/(v1[3]*v2[3]);
				if(targ>1) targ=1;
				if(targ<-1) targ=-1;
								
				double theta = acos(targ)*180/PIg;
		
				int sgn = getSgn(vecjk,v1,v2);		
				double theta2=theta;
				if(sgn<0)
					theta2 = 360-theta;
			
				for(int j=0;j<atomdih.hmany[i];j++)
				{
				
					int n = atomdih.mult[i][j];
					double eterm=0;
			
					if(n==0)  // improper dihedral
					{
						eterm = 0.5*atomdih.kchi[i][j]*pow((theta-atomdih.delta[i][j])*PIg/180,2)*econv;
						//improper+=eterm;
					}	
					else
					{
						eterm = atomdih.kchi[i][j]*(1. + cos((n*theta2 - atomdih.delta[i][j])*PIg/180))*econv;
						//proper+=eterm;
					}		
					ptot += eterm;
				}
			}
			//std::cout<<"Proper: "<<proper<<"    Improper: "<<improper<<std::endl;
			return ptot;
		}

		double pairsEnergy()
		{
			double ptot=0;
			for(int i=0;i<atompairs.npa;i++)
			{
				double dist[4];
				//fdist(coords_in[atompairs.ai[i]],coords_in[atompairs.aj[i]],dist);
				idist(atompairs.ai[i],atompairs.aj[i],dist);
				
				double pw = pow(atompairs.sigma[i]/dist[3],6);
				double eterm = 4*atompairs.eps[i]*pw*(pw - 1.)*econv;
				double cterm = (138.935485*atomtable.atomcharge[atompairs.ai[i]]*atomtable.atomcharge[atompairs.aj[i]]/dist[3])*econv;
				ptot += eterm+cterm;
			}
			//std::cout<<"Coulomb 1-4: "<<coul14<<std::endl;
			//std::cout<<"LJ 1-4: "<<ptot-coul14<<std::endl;
			return ptot;
	
		}
		
		

		double nonbondEnergy()
		{
			//double vljtot=0;
		
			double ptot=0;
			for(auto ii : atomnb.verletidx)
			{
				int i = atomnb.ai[ii];
				int j = atomnb.aj[ii];
		
				double dist[4];
				idist(i,j,dist);
				if(dist[3]>cutoff_r)
					continue;
				
				double eterm = (138.935485*atomtable.atomcharge[i]*atomtable.atomcharge[j]/dist[3])*econv;
				ptot += eterm;

				double epsi = atomtable.epsilon[i];
				double epsj = atomtable.epsilon[j];
				double sigi = atomtable.sigma[i];
				double sigj = atomtable.sigma[j];
			
				double epsij = sqrt(epsi*epsj);
				double sigij = 0.5*(sigi+sigj);
				double pw = pow(sigij/dist[3],6);
				double vterm = 4*epsij*pw*(pw - 1.)*econv;
				//vljtot += vterm;
				ptot+=vterm;
			}
			
			
			return ptot;
			
			/*for(int l=0;l<atomnb.nnb;l++)
			{
				int i = atomnb.ai[l];
				int j = atomnb.aj[l];
		
				double dist[4];
				idist(i,j,dist);
				if(dist[3]>cutoff_r)
					continue;
					
				double eterm = (138.935485*atomtable.atomcharge[i]*atomtable.atomcharge[j]/dist[3])*econv;
				ptot += eterm;

				double epsi = atomtable.epsilon[i];
				double epsj = atomtable.epsilon[j];
				double sigi = atomtable.sigma[i];
				double sigj = atomtable.sigma[j];
			
				double epsij = sqrt(epsi*epsj);
				double sigij = 0.5*(sigi+sigj);
				double pw = pow(sigij/dist[3],6);
				double vterm = 4*epsij*pw*(pw - 1.)*econv;
				//vljtot += vterm;
				ptot+=vterm;
			}
			return ptot;*/
	
			//std::cout<<"LJ nonbonded: "<<vljtot<<std::endl;
			//std::cout<<"Coulomb nonbonded: "<<ptot-vljtot<<std::endl;
			
	
		}

		
		double bondsGradient(double** grad)
		{
			double epot=0;
			
			for(int i=0;i<atombonds.nbonds;i++)
			{
				double dist[4];
				//fdist(coords_in[atombonds.ai[i]],coords_in[atombonds.aj[i]],dist);
				idist(atombonds.ai[i],atombonds.aj[i],dist);
				for(int j=0;j<atombonds.hmany[i];j++)
				{
					
					epot += 0.5*atombonds.kb[i][j]*pow(dist[3]-atombonds.b0[i][j],2)*econv;
					
					double fac = (atombonds.kb[i][j]*(dist[3]-atombonds.b0[i][j])/dist[3])*fconv;
					grad[atombonds.ai[i]][0]+=fac*dist[0];
					grad[atombonds.ai[i]][1]+=fac*dist[1];
					grad[atombonds.ai[i]][2]+=fac*dist[2];
			
					grad[atombonds.aj[i]][0]+=-fac*dist[0];
					grad[atombonds.aj[i]][1]+=-fac*dist[1];
					grad[atombonds.aj[i]][2]+=-fac*dist[2];
				}
			}
			
			return epot;
		}

		double ureybradleyGradient(double** grad)
		{
			double epot=0;
			
			for(int i=0;i<atomub.nub;i++)
			{
				double dist[4];
				//fdist(coords_in[atomub.ai[i]],coords_in[atomub.ak[i]],dist);
				idist(atomub.ai[i],atomub.ak[i],dist);
		
				for(int j=0;j<atomub.hmany[i];j++)
				{
					epot += 0.5*atomub.kub[i][j]*pow(dist[3]-atomub.rub[i][j],2)*econv;
					
					double fac = (atomub.kub[i][j]*(dist[3]-atomub.rub[i][j])/dist[3])*fconv;
					grad[atomub.ai[i]][0]+=fac*dist[0];
					grad[atomub.ai[i]][1]+=fac*dist[1];
					grad[atomub.ai[i]][2]+=fac*dist[2];
			
					grad[atomub.ak[i]][0]+=-fac*dist[0];
					grad[atomub.ak[i]][1]+=-fac*dist[1];
					grad[atomub.ak[i]][2]+=-fac*dist[2];
				}
			}
			
			return epot;
		}
		
			
		double anglesGradient(double** grad)
		{
			double epot=0;
			for(int i=0;i<atomangles.nangles;i++)
			{
				double veca[4];
				double vecb[4];
		
				//fdist(coords_in[atomangles.ai[i]],coords_in[atomangles.aj[i]],veca);
				//fdist(coords_in[atomangles.ak[i]],coords_in[atomangles.aj[i]],vecb);
				idist(atomangles.ai[i],atomangles.aj[i],veca);
				idist(atomangles.ak[i],atomangles.aj[i],vecb);
			
				
				double cdot = veca[0]*vecb[0]+veca[1]*vecb[1]+veca[2]*vecb[2];
				double G = cdot/(veca[3]*vecb[3]);
				
				if(1.0-G*G<1E-8) continue;
				
				double theta = acos(G)*180/PIg;
				double cfac = -(1./(sqrt(1.-G*G)))*180/PIg;
		
		
				for(int j=0;j<atomangles.hmany[i];j++)
				{
					double fac = atomangles.kth[i][j]*((theta-atomangles.th0[i][j])*PIg/180)*PIg/180*cfac/pow(veca[3]*vecb[3],2)*fconv;
					epot += 0.5*atomangles.kth[i][j]*pow((theta-atomangles.th0[i][j])*PIg/180,2)*econv;
			
					grad[atomangles.ai[i]][0]+=fac*(vecb[0]*veca[3]*vecb[3]-cdot*veca[0]*vecb[3]/veca[3]);
					grad[atomangles.ai[i]][1]+=fac*(vecb[1]*veca[3]*vecb[3]-cdot*veca[1]*vecb[3]/veca[3]);
					grad[atomangles.ai[i]][2]+=fac*(vecb[2]*veca[3]*vecb[3]-cdot*veca[2]*vecb[3]/veca[3]);
			
					grad[atomangles.aj[i]][0]+=fac*(-(veca[0]+vecb[0])*veca[3]*vecb[3] + cdot*(veca[0]*vecb[3]/veca[3]+vecb[0]*veca[3]/vecb[3]));
					grad[atomangles.aj[i]][1]+=fac*(-(veca[1]+vecb[1])*veca[3]*vecb[3] + cdot*(veca[1]*vecb[3]/veca[3]+vecb[1]*veca[3]/vecb[3]));
					grad[atomangles.aj[i]][2]+=fac*(-(veca[2]+vecb[2])*veca[3]*vecb[3] + cdot*(veca[2]*vecb[3]/veca[3]+vecb[2]*veca[3]/vecb[3]));
			
					grad[atomangles.ak[i]][0]+=fac*(veca[0]*veca[3]*vecb[3]-cdot*vecb[0]*veca[3]/vecb[3]);
					grad[atomangles.ak[i]][1]+=fac*(veca[1]*veca[3]*vecb[3]-cdot*vecb[1]*veca[3]/vecb[3]);
					grad[atomangles.ak[i]][2]+=fac*(veca[2]*veca[3]*vecb[3]-cdot*vecb[2]*veca[3]/vecb[3]);
				}
			}
			return epot;
		}
		
		double dihedralsGradient(double** grad)
		{
			double epot=0;
			
			for(int i=0;i<atomdih.ndih;i++)
			{
				double vecij[4];
				double vecjk[4];
				double veckl[4];
		
		
				//fdist(coords_in[atomdih.aj[i]],coords_in[atomdih.ai[i]],vecij);
				//fdist(coords_in[atomdih.ak[i]],coords_in[atomdih.aj[i]],vecjk);
				//fdist(coords_in[atomdih.al[i]],coords_in[atomdih.ak[i]],veckl);
				idist(atomdih.aj[i],atomdih.ai[i],vecij);
				idist(atomdih.ak[i],atomdih.aj[i],vecjk);
				idist(atomdih.al[i],atomdih.ak[i],veckl);
		
				double v1[4]={vecij[1]*vecjk[2]-vecij[2]*vecjk[1],vecij[2]*vecjk[0]-vecij[0]*vecjk[2],vecij[0]*vecjk[1]-vecij[1]*vecjk[0],0};
				double v2[4]={vecjk[1]*veckl[2]-vecjk[2]*veckl[1],vecjk[2]*veckl[0]-vecjk[0]*veckl[2],vecjk[0]*veckl[1]-vecjk[1]*veckl[0],0};
		
				double cdot=0;
				for(int ll=0;ll<3;ll++)
				{
					v1[3]+= v1[ll]*v1[ll];
					v2[3]+= v2[ll]*v2[ll];
					cdot += v1[ll]*v2[ll];
				}
				v1[3]=sqrt(v1[3]);
				v2[3]=sqrt(v2[3]);
		
				if(v1[3]<1E-10 || v2[3]<1E-10)
					continue;
		
				double G = cdot/(v1[3]*v2[3]);
		
				if(1-G*G<1E-8)
					continue;
		
				double theta = acos(G)*180/PIg;
				int sgn = getSgn(vecjk,v1,v2);		
				double theta2=theta;
				if(sgn<0)
					theta2 = 360-theta;
			
			
				double cfac = -(1./(sqrt(1.-G*G)))*180/PIg;		
				cfac/= pow(v1[3]*v2[3],2);
		
				// notation: x_i, y_i, z_i, x_j, ..., k ..., l
				// (Had I bothered to truly study tensor calculus I'd have probabily written it in a more elegant (but unreadable) way)
				double gradv1v2[12]= {
				(v2[3]/v1[3])*(v1[1]*vecjk[2] - v1[2]*vecjk[1]),
				(v2[3]/v1[3])*(-v1[0]*vecjk[2] + v1[2]*vecjk[0]),
				(v2[3]/v1[3])*(v1[0]*vecjk[1] - v1[1]*vecjk[0]),
				(v2[3]/v1[3])*(-v1[1]*(vecjk[2]+vecij[2])+v1[2]*(vecij[1]+vecjk[1])) + (v1[3]/v2[3])*(v2[1]*veckl[2]-v2[2]*veckl[1]),
				(v2[3]/v1[3])*(v1[0]*(vecjk[2]+vecij[2])-v1[2]*(vecij[0]+vecjk[0])) + (v1[3]/v2[3])*(-v2[0]*veckl[2]+v2[2]*veckl[0]),
				(v2[3]/v1[3])*(-v1[0]*(vecjk[1]+vecij[1])+v1[1]*(vecij[0]+vecjk[0])) + (v1[3]/v2[3])*(v2[0]*veckl[1]-v2[1]*veckl[0]),
				(v2[3]/v1[3])*(v1[1]*vecij[2]-v1[2]*vecij[1]) + (v1[3]/v2[3])*(-v2[1]*(vecjk[2]+veckl[2]) + v2[2]*(veckl[1]+vecjk[1])),
				(v2[3]/v1[3])*(-v1[0]*vecij[2]+v1[2]*vecij[0]) + (v1[3]/v2[3])*(v2[0]*(vecjk[2]+veckl[2]) - v2[2]*(veckl[0]+vecjk[0])),
				(v2[3]/v1[3])*(v1[0]*vecij[1]-v1[1]*vecij[0]) + (v1[3]/v2[3])*(-v2[0]*(vecjk[1]+veckl[1]) + v2[1]*(veckl[0]+vecjk[0])),
				(v1[3]/v2[3])*(v2[1]*vecjk[2] - v2[2]*vecjk[1]),
				(v1[3]/v2[3])*(-v2[0]*vecjk[2] + v2[2]*vecjk[0]),
				(v1[3]/v2[3])*(v2[0]*vecjk[1] - v2[1]*vecjk[0])
				};
			
		
				double gradcdot[12]={
				v2[1]*vecjk[2]-v2[2]*vecjk[1],
				-v2[0]*vecjk[2]+v2[2]*vecjk[0],
				v2[0]*vecjk[1]-v2[1]*vecjk[0],
				-v2[1]*(vecij[2]+vecjk[2]) + v2[2]*(vecjk[1]+vecij[1]) +v1[1]*veckl[2]-v1[2]*veckl[1],
				v2[0]*(vecjk[2]+vecij[2]) - v2[2]*(vecij[0]+vecjk[0]) -v1[0]*veckl[2]+v1[2]*veckl[0],
				-v2[0]*(vecjk[1]+vecij[1]) + v2[1]*(vecij[0]+vecjk[0]) +v1[0]*veckl[1]-v1[1]*veckl[0],
				v2[1]*vecij[2]-v2[2]*vecij[1] - v1[1]*(vecjk[2]+veckl[2])+v1[2]*(veckl[1]+vecjk[1]),
				-v2[0]*vecij[2]+v2[2]*vecij[0] + v1[0]*(veckl[2]+vecjk[2])-v1[2]*(vecjk[0]+veckl[0]),
				-v2[1]*vecij[0]+v2[0]*vecij[1] - v1[0]*(vecjk[1]+veckl[1])+v1[1]*(vecjk[0]+veckl[0]),
				v1[1]*vecjk[2] - v1[2]*vecjk[1],
				-v1[0]*vecjk[2] + v1[2]*vecjk[0],
				v1[0]*vecjk[1] - v1[1]*vecjk[0]
				};	
			
				for(int j=0;j<atomdih.hmany[i];j++)
				{
				
					int n = atomdih.mult[i][j];
					double fac=0;
			
					if(n==0) { // improper dihedral
						fac = (PIg/180)*atomdih.kchi[i][j]*((theta-atomdih.delta[i][j])*PIg/180)*cfac*fconv;
						epot += 0.5*atomdih.kchi[i][j]*pow((theta-atomdih.delta[i][j])*PIg/180,2)*econv;
					}
					else{
						fac = -sgn*(PIg/180)*atomdih.kchi[i][j]*sin((n*theta2 - atomdih.delta[i][j])*PIg/180)*n*cfac*fconv;
						epot += atomdih.kchi[i][j]*(1. + cos((n*theta2 - atomdih.delta[i][j])*PIg/180))*econv;
					}
			
			
			
					for(int d=0;d<3;d++)
					{
						grad[atomdih.ai[i]][d]+=fac*(gradcdot[0+d]*v1[3]*v2[3]-cdot*gradv1v2[0+d]);
						grad[atomdih.aj[i]][d]+=fac*(gradcdot[3+d]*v1[3]*v2[3]-cdot*gradv1v2[3+d]);
						grad[atomdih.ak[i]][d]+=fac*(gradcdot[6+d]*v1[3]*v2[3]-cdot*gradv1v2[6+d]);
						grad[atomdih.al[i]][d]+=fac*(gradcdot[9+d]*v1[3]*v2[3]-cdot*gradv1v2[9+d]);
					}
			
				}
			}
			return epot;
	
		}
		
		double pairsGradient(double** grad)
		{
			double epot=0;
			
			for(int i=0;i<atompairs.npa;i++)
			{
				double dist[4];
				//fdist(coords_in[atompairs.ai[i]],coords_in[atompairs.aj[i]],dist);
				idist(atompairs.ai[i],atompairs.aj[i],dist);
				
				double pw = pow(atompairs.sigma[i]/dist[3],6);
				double fac = (24*atompairs.eps[i]*pw*(1.-2*pw)/dist[3])*fconv;
				double cterm = -(138.935485*atomtable.atomcharge[atompairs.ai[i]]*atomtable.atomcharge[atompairs.aj[i]]/(dist[3]*dist[3]))*fconv;
				
				double eterm = 4*atompairs.eps[i]*pw*(pw - 1.)*econv;
				double encterm = (138.935485*atomtable.atomcharge[atompairs.ai[i]]*atomtable.atomcharge[atompairs.aj[i]]/dist[3])*econv;
				
				epot += eterm+encterm;
				for(int d=0;d<3;d++)
				{
					grad[atompairs.ai[i]][d]+=(fac+cterm)*dist[d]/dist[3];
					grad[atompairs.aj[i]][d]+=-(fac+cterm)*dist[d]/dist[3];
				}
			}
			
			return epot;
		}
		
		double nonbondGradient(double** grad)
		{
			double epot=0;
			
			//for(int l=0;l<atomnb.nnb;l++)
			
			for(auto l : atomnb.verletidx)
			{
				int i = atomnb.ai[l];
				int j = atomnb.aj[l];
		
				double dist[4];
				//fdist(coords_in[i],coords_in[j],dist);
				idist(i,j,dist);
				if(dist[3]>cutoff_r)
					continue;
				
				double epsi = atomtable.epsilon[i];
				double epsj = atomtable.epsilon[j];
				double sigi = atomtable.sigma[i];
				double sigj = atomtable.sigma[j];
			
				double epsij = sqrt(epsi*epsj);
				double sigij = 0.5*(sigi+sigj);
				double pw = pow(sigij/dist[3],6);
			
				double fac = (24*epsij*pw*(1.-2*pw)/dist[3])*fconv;
				double cterm = -(138.935485*atomtable.atomcharge[i]*atomtable.atomcharge[j]/(dist[3]*dist[3]))*fconv;
				
				double eterm = (138.935485*atomtable.atomcharge[i]*atomtable.atomcharge[j]/dist[3])*econv;
				double vterm = 4*epsij*pw*(pw - 1.)*econv;
				epot += eterm+vterm;
				
				for(int d=0;d<3;d++)
				{
					grad[i][d]+=(fac+cterm)*dist[d]/dist[3];
					grad[j][d]+=-(fac+cterm)*dist[d]/dist[3];
				}
			}
			return epot;
	
		}

		
		// project on the plane perpendicular to the backbone vector and determine the sign of the angle between n1 and n2
		int getSgn(double vecjk[], double n1[], double n2[])  // sign of the angle between v1 and v2. Positive = clockwise. (dihedral angles standard)
		{
	
	
			double a[4];
	
			a[0]=vecjk[0]/vecjk[3];
			a[1]=vecjk[1]/vecjk[3];
			a[2]=vecjk[2]/vecjk[3];
			a[3]=1;
	
			double b[4]={1,1,1,0};
	
			if(fabs(a[0])>1E-10)
				b[0]= -(a[1]+a[2])/a[0];
			else if(fabs(a[1])>1E-10)
				b[1]= -(a[0]+a[2])/a[1];
			else
				b[2]= -(a[0]+a[1])/a[2];
	
			b[3]=sqrt(b[0]*b[0]+b[1]*b[1]+b[2]*b[2]);
			b[0]/=b[3];
			b[1]/=b[3];
			b[2]/=b[3];
	
			double c[4]={0,0,0,1}; // already normalized by vector product
			c[0] = a[1]*b[2]-a[2]*b[1];
			c[1] = a[2]*b[0]-a[0]*b[2];
			c[2] = a[0]*b[1]-a[1]*b[0];
	
	
			double n1plane[2]={n1[0]*b[0]+n1[1]*b[1]+n1[2]*b[2],n1[0]*c[0]+n1[1]*c[1]+n1[2]*c[2]};
			double n2plane[2]={n2[0]*b[0]+n2[1]*b[1]+n2[2]*b[2],n2[0]*c[0]+n2[1]*c[1]+n2[2]*c[2]};
	
			double vz =n1plane[0]*n2plane[1]-n1plane[1]*n2plane[0];
	
			if(vz>0)
				return 1;
			else
				return -1;
		}

		
		
		void pbc_ortho(double* a, double* b, double pbd[])
		{
			pbd[3]=0;
	
			for(int j=0;j<3;j++)
			{
				double val = a[j]-b[j];
				if(fabs(val)<L[j]/2)
					pbd[j] =val;
				else
				{
					if(val>0)
						pbd[j] = val - L[j];
					else
						pbd[j] = val + L[j];
				}
		
				pbd[3]+= pbd[j]*pbd[j];
			}
			pbd[3] = sqrt(pbd[3]);
		}

		void no_pbc(double* a, double* b, double pbd[])
		{
			pbd[3]=0;
			for(int i=0;i<3;i++)
			{
				pbd[i]= a[i]-b[i];
				pbd[3]+= pbd[i]*pbd[i];
			}
			pbd[3]= sqrt(pbd[3]);
		}

		void fdist(double* a,double* b,double pbd[])
		{
			if(pbc)
				pbc_ortho(a,b,pbd);
			else
				no_pbc(a,b,pbd);
		}
		
		void idist(int i, int j, double pbd[])
		{
			
			int sgn=1;
			int ai = i;
			int aj = j;
			if(i < j)
			{
				ai=j;
				aj=i;
				sgn=-1;
			}
			
			
			pbd[0] = sgn*dist_table[ai][aj][0];
			pbd[1] = sgn*dist_table[ai][aj][1];
			pbd[2] = sgn*dist_table[ai][aj][2];
			pbd[3] = dist_table[ai][aj][3];
			//std::cout<<pbd[0]<<std::endl;
			
			
		}
		
		
		std::string topo_file;
		std::string ff_basedir;
		double econv,fconv;
		bool silent;
	
};

typedef struct Bonds Bonds;
typedef struct UreyBradley UreyBradley;
typedef struct Angles Angles;
typedef struct Atomtable Atomtable;
typedef struct Dihedrals Dihedrals;
typedef struct Nonbonded Nonbonded;
typedef class oorcharmm oorcharmm;
typedef struct Pairs Pairs;

#endif
