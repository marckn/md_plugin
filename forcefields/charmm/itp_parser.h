#ifndef __ITPPARSER__
#define __ITPPARSER__

#include<iostream>
#include<sstream>
#include<fstream>
#include<string>
//#include "string.h"

// ignores ; and # lines

struct itp_element
{
	std::string** elements;
	int nout;
	int ncols;
};
void free_itp_element(itp_element& src)
{
	if(src.nout==0)
		return;
	
	for(int i=0;i<src.nout;i++)
		delete[] src.elements[i];

	delete[] src.elements;

}
struct itp_parser
{
	
		void init(const char* _filename, bool _silent=true)
		{
			silent=_silent;
			if(!silent)
				std::cout<<"itp_parser(): parsing "<<_filename<<std::endl;
	
			filename = _filename;
			std::ifstream inp(_filename);
			if(!inp.is_open())
			{
				std::string errm= "itp_parser(): ERROR. Cannot open "+std::string(_filename);
				throw std::runtime_error(errm);
			}
			char buff[2000];
			nlines=0;
			nlenmax=0;
		
			while(!inp.eof())
			{
				inp.getline(buff,2000);
				int nlen = inp.gcount();
					if(nlenmax<nlen)
					nlenmax=nlen;
		
				if(buff[0]!=';' && buff[0]!='#' && strlen(buff)!=0)
					nlines++;
			}
			if(!silent)
				std::cout<<"itp_parser(): read "<<nlines<<" non-comment lines with max number of chars of "<<nlenmax<<std::endl;
	
			//buffer= new std::string[nlines];
	
			inp.close();
			inp.open(_filename);
	
			//int knt=0;
	
			while(!inp.eof())
			{
				inp.getline(buff,2000);
				if(buff[0]!=';' && buff[0]!='#' && strlen(buff)!=0)
				{
					buffer.push_back(buff);
					//buffer[knt] = buff;
					//knt++;
				}
			}
			inp.close();
		
			if(!silent)
				std::cout<<"itp_parser(): Input file scanned"<<std::endl;
	

		}
		
		
		/*~itp_parser()
		{
			if(nlines==0)
				return;
		
			delete[] buffer;
		}*/
		
		void getElement(const char *keyword, int ncols, itp_element& elm)
		{
				elm.nout=0;
			bool accumulate=false;
	
			for(int i=1;i<nlines;i++)
			{
				if(buffer[i-1]==keyword)
					accumulate=true;
				if(buffer[i][0]=='[')
					accumulate=false;
		
				if(accumulate)
				{
					elm.nout++;
				}
			}
	
			int idx=0;
			elm.elements = new std::string*[elm.nout];
			elm.ncols=ncols;
			accumulate=false;
	
			for(int i=1;i<nlines;i++)
			{
				if(buffer[i-1]==keyword)
					accumulate=true;
				if(buffer[i][0]=='[')
					accumulate=false;
		
				if(accumulate)
				{
					elm.elements[idx] = new std::string[ncols];
					std::stringstream values;
					values<<buffer[i];
					for(int j=0;j<ncols;j++)
					{
						if(values.eof())
						{
							elm.elements[idx][j]="";
						}
						else
							values>>elm.elements[idx][j];
					}
					idx++;
				}
			}
		}
	
	
		std::string filename;
		std::vector<std::string> buffer;
		int nlines, nlenmax;
		bool silent;
	
};

typedef struct itp_element itp_element;
typedef class itp_parser itp_parser;

#endif
