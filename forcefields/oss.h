#ifndef __OSS__
#define __OSS__

#define OSS1 1
#define OSS2 2
#define OSS3 3


#include<armadillo>
#include<string>
#include<cmath>

#include<fstream>
struct POSS
{
	void init(int _nO, int _nH, std::string _type="OSS3",bool _preliminary=true)
	{
		usePBC=false;
		L[0]=L[1]=L[2]=0;
		type=_type;
		preliminary=_preliminary;
		field_done=false;
		nH=_nH;
		nO=_nO;
		ntot=nH+nO;
		Vel=V2=V3B=Voss3=0;		
		Dcoupling.zeros(3*nO,3*nO);
		
		Efield.zeros(3*nO);
		muvec.zeros(nO,3);
		
		en_atom.zeros(nO+nH);
		FO.zeros(nO,3);
		FH.zeros(nH,3);
		Ftot.zeros(nO+nH,3);
			

		r0=theta0=0;
		mu0=dpar=alpha=0;
		a[0]=a[1]=a[2]=0;
		b[0]=b[1]=b[2]=0;
		c[0]=c[1]=c[2]=0;
		for(int i=0;i<6;i++)
			h[i]=0;
			
		for(int i=0;i<8;i++)
			o[i]=0;
		
		for(int i=0;i<17;i++)
			k[i]=0;

		alpha=1.444/(a0*a0*a0);
		r0=0.9614;
		theta0=1.81733805;
		int itype=1;
		if(_type=="OSS2")
			itype=2;
		if(_type=="OSS3")
			itype=3;
		
	
		switch(itype)
		{
			case OSS1:
			{
				a[1]=1.89553914/(a0*a0);
				a[2]=0.39027508*a0;
				h[1]=0.00604105;
				h[2]=2.70935140;
				h[3]=1.03077240;
				h[4]=3.17840900;
				h[5]=0.08638155;
				o[1]=57.57947120;
				o[2]=1.18162620;
				o[3]=-57.97837320;
				o[4]=1.18960120;
				o[5]=0.11779740;
				o[6]=4.90485888;
				o[7]=1.66494610;
				k[1]=-0.04090215;
				k[2]=0.15742935;
				k[3]=-0.02626975;
				k[4]=-0.37847190;
				k[5]=-0.34038010;
				k[6]=-0.00917083;
				k[7]=0.07395235;
				k[8]=0.53031285;
				k[9]=1.20321380;
				k[10]=0.07190275;
				k[11]=-0.20913555;
				k[12]=-0.07793255;
				k[13]=-0.07518620;
				k[14]=-0.27656035;
				if(preliminary)
					k[15]=-4.0986179;
				
				k[16]=-0.03059900;
				m[1]=6.25; m[2] = 0.00163726; 
				
				
				if(preliminary)
					m[3]=0.0000055;
				else
					m[3] = 6.25;
					
			
				break;
			}
			case OSS2:
			{
				a[1]=1.73089134/(a0*a0); a[2]=0.29575999*a0;
				b[1]=332.28529224/(a0*a0);
				b[2]=2.84683483*a0;
				c[1]=1.09568640/(a0*a0);
				c[2]=0.00000204*a0;
				h[1]=0.00575558;
				h[2]=2.64253740;
				h[3]=1.12743850;
				h[4]=3.32556550;
				h[5]=0.07847734;
				o[1]=40.48587340;
				o[2]=1.32909550;
				o[3]=-41.72606080;
				o[4]=1.35094910;
				o[5]=0.06293960;
				o[6]=6.77909903;
				o[7]=1.81178360;
				k[1]=-0.04200730;
				k[2]=0.16488265;
				k[3]=-0.02509795;
				k[4]=-0.37814525;
				k[5]=-0.31667595;
				k[6]=-0.01146720;
				k[7]=0.06061075;
				k[8]=0.52828100;
				k[9]=1.16176270;
				k[10]=0.07652320;
				k[11]=-0.21208835;
				k[12]=-0.10253850;
				k[13]=-0.07622160;
				k[14]=-0.22869400;
				if(preliminary)
					k[15]=-3.6629465;
					
				k[16]=-0.02909200;
				
				m[1]=6.25;
				m[2]=0.00377233;
				if(preliminary)
					m[3]=0.0000817;
				else
					m[3]=6.25;
					
				break;
			}
			case OSS3:
			{
				a[1]=2.11898470/(a0*a0);
				a[2]=0.50618287*a0;
				b[1]=23.65836273/(a0*a0);
				b[2]=21.45748807*a0;
				c[1]=0.54212046/(a0*a0);
				c[2]=0.00000000*a0;
				h[1]=0.00133447;
				h[2]=3.01516330;
				h[3]=1.28643820;
				h[4]=3.53717380;
				h[5]=0.04262954;
				o[1]=0.04665470;
				o[2]=0.74639650;
				o[3]=0.10829120;
				o[4]=0.96360050;
				o[5]=0.07127660;
				o[6]=6.44074302;
				o[7]=1.78031300;
				k[1]=-0.04610130;
				k[2]=0.16553720;
				k[3]=-0.00605500;
				k[4]=-0.41011195;
				k[5]=-0.34458595;
				k[6]=-0.00412615;
				k[7]=0.04878605;
				k[8]=0.52945720;
				k[9]=0.65695540;
				k[10]=0.07707450;
				k[11]=-0.08696845;
				k[12]=0.50608900;
				k[13]=-0.09358865;
				k[14]=-0.28291475;
				if(preliminary)
					k[15]=-3.3400269;
				
				k[16]=-0.04702270;
				m[1]=6.25;
				m[2]=0.00000048;
				if(preliminary)
					m[3]=0.00000000;
				else
					m[3]=4.41;
				
				p[1]=0.02023165;
				p[2]=-0.06380331;
				p[3]=-0.25846763;
				p[4]=0.04270098;
				p[5]=0.04903794;
				p[6]=-0.96744972;
				p[7]=0.03626479;
				p[8]=1.77639113;
				p[9]=-0.11463702;
				p[10]=0.04043341;
				p[11]=-2.62366228;
				p[12]=-0.05729762;
				mu0=-0.79734654/a0;
				dpar=2.25*a0*a0;
			
				break;
			}
			default:
			{
				std::cerr<<"ERROR: unrecognized OSS potential type"<<std::endl;
				exit(1);
			}
		
			
		}
	
	
	
	}	
	
		double feedCoordinates(arma::mat _rO, arma::mat _rH)
		{
			en_atom.zeros();
			Vel=V2=V3B=Voss3=0;
			field_done=false;
			rO = _rO;
			rH = _rH;
			rtotal=arma::join_cols(rO,rH);
			
			
			electricField();
			electrostatics();
			
			V2body();
			V3body();
			
			if(type=="OSS3")
				V3bdipole();
			
			return energy();
			
		}

		
		void computeForces()
		{
		 	FO.zeros();
			FH.zeros();
	
			if(!field_done)
				electricField();
				
			electrostaticForces();
			forces2B();
			forces3B();
			
			if(type=="OSS3")
				forcesOSS3();  
				
			
			FO = -FO;
			FH = -FH;
			
			Ftot = arma::join_cols(FO,FH);
			
		}
		
		
		double Vel, V2, V3B, Voss3;
		arma::mat FO, FH, Ftot;   // forces on O, H and O+H
		
		double energy(){return Vel+V2+V3B+Voss3;}
		arma::mat& forces(){return Ftot;}
		void setPBC(double Lx, double Ly, double Lz){
			L[0]=Lx;
			L[1]=Ly;
			L[2]=Lz;
			usePBC=true;
		}
		
		void unsetPBC() {usePBC=false;}
		
	
	 	
		// pbc variables
		bool usePBC;
		double L[3];
		
		std::string type;
		bool preliminary;
		arma::mat rO, rH,rtotal;
		arma::vec en_atom;
		
		arma::vec Efield,mustrip;
		arma::mat Dcoupling,Dinverse, muvec;
		
		int nH,nO,ntot;
		bool field_done;  // flag for electric field computation
		double r0,theta0;
		double a[3], b[3], c[3], h[6],o[8],k[17];  // zero element never used. Only to keep the same indices as in the paper.
		double m[4],p[13];
		double mu0,d,alpha;
		
		double qO=-2;
		double qH=1;
		double a0 = 0.529177;  // bohr radius
		double dpar=0;
	
		// Switching and cutoff functions
		double Sohcd(double r){
			return r*r/(r*r+a[1]*exp(-a[2]*r));}
		
		double Soocd(double r){
			return r*r/(r*r+b[1]*exp(-b[2]*r));}
		
		double Soodd(double r){
			return r*r/(r*r+c[1]*exp(-c[2]*r));}
			
		double fcutoff(double r1, double r2, double theta){
			double dr1 = r1-r0;
			double dr2 = r2-r0;
			double dtheta = theta - theta0;
			double ee = m[1]*(dr1*dr1+dr2*dr2);
			ee += m[2]*dtheta*dtheta;
			ee += m[3]*(dr1*dr1+dr2*dr2)*dtheta*dtheta;
			return exp(-ee);
		}
		
		double gdipole(double muksquare){
			return (muksquare-mu0*mu0)/(muksquare+mu0*mu0 + dpar*muksquare*muksquare);
		}
		
		
		double dist(const arma::rowvec& r1,const arma::rowvec& r2)
		{
			arma::rowvec rdiff;
			return diff(r1,r2,rdiff);
			
			//double rq = arma::dot(rdiff,rdiff);  
			//return sqrt(rq);
		}
		
		double diff(const arma::rowvec& r1,const arma::rowvec& r2, arma::rowvec& rdiff)
		{
			rdiff = r1 - r2;
			
			if(usePBC)
				for(int j=0;j<3;j++)
				{
					double val = rdiff(j);
					if(fabs(val)<L[j]/2)
						;
					else
					{
						if(val>0)
							rdiff(j) = val - L[j];
						else
							rdiff(j) = val + L[j];
					}
				}
			return sqrt(arma::dot(rdiff,rdiff));
		}
		
		
		double dot(const arma::rowvec& r1, const arma::rowvec& r2)
		{
			
			double dp=0;
			dp += r1[0]*r2[0] + r1[1]*r2[1] + r1[2]*r2[2];
			return dp;
			
		}


void electricField()
{
	// Electric field and dipole couplings
	Efield.zeros();
	Dcoupling.zeros();
			
	for(int i=0;i<nO;i++)
	{
		for(int j=0;j<i;j++)
		{
				
			arma::rowvec rijdiff;
			double dij = diff(rO.row(j),rO.row(i),rijdiff)/a0;   // j - i
			
			double dij2 = pow(dij,2);
			double dij3 = dij2*dij;
			double coff=Soocd(dij);
			
	
			for(int k=0;k<3;k++)
			{
				Efield[i+k*nO]+=-qO*coff*rijdiff(k)/(dij3*a0);
				Efield[j+k*nO]-=-qO*coff*rijdiff(k)/(dij3*a0);
			}
		
			
			// Dipole-dipole coupling matrix (one mat object for each space direction)
			double dcoff = Soodd(dij);
			double fc = -3.0/dij2;     //pow(dij,5);
			
			for(int k1=0;k1<3;k1++)
			{
				for(int k2=0;k2<3;k2++)
				{
					Dcoupling(i+k1*nO,j+k2*nO) += -fc*rijdiff(k1)*rijdiff(k2)*dcoff/(dij3*a0*a0);
					Dcoupling(j+k1*nO,i+k2*nO) += -fc*rijdiff(k1)*rijdiff(k2)*dcoff/(dij3*a0*a0);
				}
				Dcoupling(i+k1*nO,j+k1*nO) += -dcoff/dij3;
				Dcoupling(j+k1*nO,i+k1*nO) += -dcoff/dij3;
				
			}
			
			
			
		
		}
		for(int j=0;j<nH;j++)
		{
			arma::rowvec rijdiff;
			double dij = diff(rH.row(j),rO.row(i),rijdiff)/a0;   
			
			double coff=Sohcd(dij);
			
			for(int k=0;k<3;k++)
				Efield[i+k*nO]+=-qH*coff*rijdiff(k)/(pow(dij,3)*a0);
			
			
			
		}
	
	}
	
	arma::mat Id = arma::mat(nO*3,nO*3,arma::fill::eye);
	
	arma::mat tinv = (Id - alpha*Dcoupling).i();
	Dinverse = tinv;
	
	mustrip = alpha*tinv*Efield;
	
	for(int i=0;i<nO;i++)
	{
		muvec(i,0)=mustrip[i];
		muvec(i,1)=mustrip[i+nO];
		muvec(i,2)=mustrip[i+2*nO];
	}
	
	field_done=true;
	

}


void electrostatics()
{
	// From optimized dipole moments compute the electrostatic part of the interacting potential
	// For clarity I'll keep the order of the terms as in Eq. (1)
	
	Vel=0;
	
	for(int i=0;i<nO;i++){
		for(int j=0;j<i;j++)
		{
			arma::rowvec rijdiff;
			double dij = diff(rO.row(j),rO.row(i),rijdiff)/a0;
			double dij2=dij*dij;
			double dij3=dij2*dij;
			double coff = Soodd(dij);
			double t1 = dot(muvec.row(i),rijdiff)/a0;
			double t2 = dot(muvec.row(j),rijdiff)/a0;
			double Tij = dot(muvec.row(i),muvec.row(j))-3*t1*t2/dij2;
			Vel += Tij*coff/dij3;
			
			en_atom(i) += Tij*coff/dij3;
			en_atom(j) += Tij*coff/dij3;
			
			
			// Second term in eq. (1), first part
			double coffcd = Soocd(dij);
			Vel += -qO*t2*coffcd/dij3;
			
			 en_atom(i) += -qO*t2*coffcd/dij3;
			 en_atom(j) += -qO*t2*coffcd/dij3;
			
		}
		
		for(int j=i+1;j<nO;j++)
		{
			// Second term in eq. (1), second part
			arma::rowvec rijdiff;
			double dij = diff(rO.row(j),rO.row(i),rijdiff)/a0;
			double dij3=dij*dij*dij;
			double t = dot(muvec.row(j),rijdiff)/a0;
			double coffcd = Soocd(dij);
			Vel += -qO*t*coffcd/dij3;
			en_atom(i) += -qO*t*coffcd/dij3;
			en_atom(j) += -qO*t*coffcd/dij3;
		}
	}
	
	for(int i=0;i<nH;i++)
		for(int j=0;j<nO;j++)
		{
			// Last part of second term in eq. 1
			
			arma::rowvec rijdiff;
			double dij = diff(rO.row(j),rH.row(i),rijdiff)/a0;
			double dij3 = dij*dij*dij;
			double t = dot(muvec.row(j),rijdiff)/a0;
			double coffcd = Sohcd(dij);
			Vel += -qH*t*coffcd/dij3;
			
			en_atom(i+nO) += -qH*t*coffcd/dij3;
			en_atom(j) += -qH*t*coffcd/dij3;
		}
	
	
	
	
	
	
	for(int i=0;i<nO;i++)
		for(int j=0;j<i;j++)
		{
			double dij = dist(rO.row(i),rO.row(j))/a0;
			Vel += qO*qO/dij;
			en_atom(i) += qO*qO/dij;
			en_atom(j) += qO*qO/dij;
		}
		
	for(int i=0;i<nH;i++)
		for(int j=0;j<i;j++)
		{
			double dij = dist(rH.row(i),rH.row(j))/a0;
			Vel += qH*qH/dij;
			en_atom(nO+i) += qH*qH/dij;
			en_atom(nO+j) += qH*qH/dij;
		}
	
	for(int i=0;i<nO;i++)
		for(int j=0;j<nH;j++)
		{
			double dij = dist(rO.row(i),rH.row(j))/a0;
			Vel += qO*qH/dij;
			en_atom(i) += qO*qH/dij;
			en_atom(nO+j) += qO*qH/dij;
		}
	
	for(int i=0;i<nO;i++)
	{
		double mquad = dot(muvec.row(i),muvec.row(i));
		Vel += mquad/(2*alpha);
		en_atom(i) += mquad/(2*alpha);
	}
	
	
}

 void V2body()
{
	V2=0;
	for(int i=0;i<nO;i++)
	{
		for(int j=0;j<i;j++)
		{
			double dij = dist(rO.row(i),rO.row(j));
			double cv = o[1]*exp(-o[2]*dij);
			cv += o[3]*exp(-o[4]*dij);
			cv += o[5]*exp(-o[6]*pow(dij-o[7],2));
			V2 += cv;
			en_atom(i) += cv;
			en_atom(j) += cv;
		}
		
		for(int j=0;j<nH;j++)
		{
			double dij = dist(rO.row(i),rH.row(j));
			double t1 = pow(1.0-h[5],2)*exp(-h[3]*(dij-h[2]))/(pow(1-h[5],2)+h[5]*h[5]);
			double t2 = h[5]*h[5]*exp(-h[4]*(dij-h[2]))/(pow(1-h[5],2)+h[5]*h[5]);
			V2 += h[1]*(pow(1-t1-t2,2)-1);
			en_atom(i) += h[1]*(pow(1-t1-t2,2)-1);
			en_atom(j+nO) += h[1]*(pow(1-t1-t2,2)-1);
		}
	}
}

 void V3body()
{
	V3B=0;
	for(int i=0;i<nO;i++)
		for(int j=0;j<nH;j++)
			for(int kk=0;kk<j;kk++)
			{
				
				arma::rowvec rijdiff;
				double r1 = diff(rH.row(j),rO.row(i),rijdiff);
				double dr1 = r1-r0;
				
				arma::rowvec rikdiff;
				double r2 = diff(rH.row(kk),rO.row(i),rikdiff);
				double dr2 = r2-r0;
				
				double dtp = dot(rijdiff,rikdiff);
				
				double tfac = dtp/(r1*r2);
				if(tfac>1) tfac=1.0;
				if(tfac<-1) tfac=-1.0;
					
				double theta=acos(tfac);
					
				double dtheta = theta - theta0;
				
				double coff = fcutoff(r1,r2,theta);
				
				double tacc=k[1] + k[2]*(dr1+dr2) + k[3]*dtheta;
				
				tacc += k[4]*(dr1*dr1+dr2*dr2)+k[5]*dr1*dr2+k[6]*dtheta*dtheta+k[7]*(dr1+dr2)*dtheta;
				tacc += k[8]*(dr1*dr1*dr1+dr2*dr2*dr2) + k[9]*(dr1*dr1*dr2+dr1*dr2*dr2) + k[10]*pow(dtheta,3);
				tacc += k[11]*(dr1*dr1+dr2*dr2)*dtheta + k[12]*dr1*dr2*dtheta + k[13]*(dr1+dr2)*dtheta*dtheta;
				tacc += k[14]*(pow(dr1,4)+pow(dr2,4)) + k[15]*pow(dr1*dr2,2) + k[16]*pow(dtheta,4);
				V3B += coff*tacc;
				en_atom(i) += coff*tacc;
				en_atom(j+nO) += coff*tacc;
				en_atom(kk+nO) += coff*tacc;
			}
	
}
 void V3bdipole()
{
	Voss3=0;
	for(int i=0;i<nO;i++)
		for(int j=0;j<nH;j++)
			for(int kk=0;kk<j;kk++)
			{
				arma::rowvec rijdiff;
				double r1 = diff(rH.row(j),rO.row(i),rijdiff);
				double dr1 = r1-r0;
				arma::rowvec rikdiff;
				double r2 = diff(rH.row(kk),rO.row(i),rikdiff);
				double dr2 = r2-r0;
				
				double dtp = dot(rijdiff,rikdiff);
				double tfac = dtp/(r1*r2);
				if(tfac>1) tfac = 1;
				if(tfac<-1) tfac=-1;
				
				double theta = acos(tfac);
				double dtheta = theta - theta0;
				
				double coff = fcutoff(r1,r2,theta);
				double musquare = dot(muvec.row(i),muvec.row(i));
				double gmucut = gdipole(musquare);
				double tacc = p[1] + p[2]*dtheta + p[3]*dr1*dr2 + p[4]*dtheta*dtheta + p[5]*(dr1+dr2)*dtheta;
				tacc += p[6]*(dr1*dr1*dr2 + dr1*dr2*dr2) + p[7]*(dr1*dr1+dr2*dr2)*dtheta + p[8]*dr1*dr2*dtheta;
				tacc += p[9]*(dr1+dr2)*dtheta*dtheta + p[10]*pow(dtheta,3) + p[11]*pow(dr1*dr2,2) + p[12]*pow(dtheta,4);
				Voss3 += tacc*gmucut*coff;
				en_atom(i) += tacc*gmucut*coff;
				en_atom(j+nO) += tacc*gmucut*coff;
				en_atom(kk+nO) += tacc*gmucut*coff;
			
			}


}




double dSohcd(double r){
	double den = (r*r+a[1]*exp(-a[2]*r));
	double t1= 2*r/den;
	double t2 = r*r*(2*r-a[1]*a[2]*exp(-a[2]*r))/(den*den);
	return t1-t2;
	
}


double dSoocd(double r){
	double den = (r*r+b[1]*exp(-b[2]*r));
	double t1= 2*r/den;
	double t2 = r*r*(2*r-b[1]*b[2]*exp(-b[2]*r))/(den*den);
	return t1-t2;
	
}		
double dSoodd(double r){
	double den = (r*r+c[1]*exp(-c[2]*r));
	double t1= 2*r/den;
	double t2 = r*r*(2*r-c[1]*c[2]*exp(-c[2]*r))/(den*den);
	return t1-t2;
	
}			



void electrostaticForces()
{
	for(int i=0;i<nO;i++)
	{
		for(int j=0;j<i;j++)
		{
			// first term derivative: O vs O
			
			arma::rowvec rijdiff;
			double rij = diff(rO.row(j),rO.row(i),rijdiff)/a0;
			double rij2 = rij*rij;
			double rij3 = rij2*rij;
			double rij4 = rij3*rij;
			double rij5 = rij4*rij;
			
			
			double t1 = dot(muvec.row(i),rijdiff)/a0;
			double t2 = dot(muvec.row(j),rijdiff)/a0;
			double Tprod = dot(muvec.row(i),muvec.row(j)) - 3.0*t1*t2/rij2;
			double coff = Soodd(rij);
			double dcoff = dSoodd(rij);
			
			double coffcd = Soocd(rij);
			double dcoffcd = dSoocd(rij);
			
			for(int d=0;d<3;d++)
			{
				
				double t = -t1*t2*6*rijdiff(d)/(rij4*a0);
				t+= (3.0/rij2)*(t1*muvec(j,d)+ t2*muvec(i,d));
				
				FO(i,d) += coff*t/rij3;
				FO(j,d) -= coff*t/rij3;
				
				
				// second part
				t = 3*coff*rijdiff(d)/(rij5*a0);
				t+= -dcoff*rijdiff(d)/(rij4*a0);
				FO(i,d) += Tprod*t;
				FO(j,d) -= Tprod*t;
			
				// second term derivative: O vs O
				
				// In this part r_ji gives + sign when derived
				t = muvec(j,d)*coffcd/rij3;  
				t += -t2*(3.0*coffcd*rijdiff(d)/(rij5*a0));
				t += t2*dcoffcd*rijdiff(d)/(rij4*a0);

				FO(i,d) += qO*t;
				FO(j,d) -= qO*t;
				
				
				// O-O bare part
				t = rijdiff(d)/(rij3*a0);
				FO(i,d) += qO*qO*t;
				FO(j,d) -= qO*qO*t;
				
			
			}
		}
		
		for(int j=i+1;j<nO;j++)
		{
			// second term derivative O vs O, part two
			arma::rowvec rijdiff;
			double rij = diff(rO.row(j),rO.row(i),rijdiff)/a0;
			double rij3 = rij*rij*rij;
			double rij4 = rij3*rij;
			double rij5 = rij4*rij;
			double t2 = dot(muvec.row(j),rijdiff)/a0;
			double coffcd = Soocd(rij);
			double dcoffcd = dSoocd(rij);
			
			for(int d=0;d<3;d++)
			{
				double t = muvec(j,d)*coffcd/rij3;
				t += -t2*(3.0*coffcd*rijdiff(d)/(rij5*a0));
				t += t2*dcoffcd*rijdiff(d)/(rij4*a0);
				
				FO(i,d) += qO*t;
				FO(j,d) -= qO*t;

			}
		}
		
		for(int j=0;j<nH;j++)
		{
			arma::rowvec rijdiff;
			double rij = diff(rH.row(j),rO.row(i),rijdiff)/a0;
			double rij2 = rij*rij;
			double rij3 = rij2*rij;
			double rij4 = rij3*rij;
			double rij5 = rij4*rij;
			
			double t1 = dot(muvec.row(i),rijdiff)/a0;
			
			double coffcd = Sohcd(rij);
			double dcoffcd = dSohcd(rij);
			
			for(int d=0;d<3;d++)
			{
				// O vs H: dipole term
			
				double t = -muvec(i,d)*coffcd/rij3;
				t += 3*t1*coffcd*rijdiff(d)/(rij5*a0);
				t += -t1*dcoffcd*rijdiff(d)/(rij4*a0);
				FO(i,d) += qH*t;
				FH(j,d) -= qH*t;
			
				// O vs H: bare part
				FO(i,d) += qO*qH*rijdiff(d)/(rij3*a0);
				FH(j,d) -= qO*qH*rijdiff(d)/(rij3*a0);
				
			}
		}
	}
	
	// H vs H bare part
	
	for(int i=0;i<nH;i++)
		for(int j=0;j<i;j++)
		{
			arma::rowvec rijdiff;
			double rij = diff(rH.row(j),rH.row(i),rijdiff)/a0;
			double rij3 = rij*rij*rij;
			for(int d=0;d<3;d++)
			{
				FH(i,d) += qH*qH*rijdiff(d)/(rij3*a0);
				FH(j,d) -= qH*qH*rijdiff(d)/(rij3*a0);
			
			}
		}
		
	// d/dr here is in bohr units, need to change it to angstroms:
	FH = FH/a0;
	FO = FO/a0;
	
}

void forces2B()
{
	for(int i=0;i<nO;i++)
	{
		// O vs O part
		for(int j=0;j<i;j++)
		{
			arma::rowvec rijdiff;
			double rij = diff(rO.row(j),rO.row(i),rijdiff);
			
			double deriv = -o[1]*o[2]*exp(-o[2]*rij);
			deriv += -o[3]*o[4]*exp(-o[4]*rij);
			deriv += -2*o[5]*o[6]*(rij-o[7])*exp(-o[6]*pow(rij-o[7],2));
			for(int d=0;d<3;d++)
			{
				FO(i,d) += -deriv*rijdiff(d)/rij;
				FO(j,d) -= -deriv*rijdiff(d)/rij;
			}
		}
		
		// O vs H part
		for(int j=0;j<nH;j++)
		{
			arma::rowvec rijdiff;
			double rij = diff(rH.row(j),rO.row(i),rijdiff);
			double den = pow(1.0-h[5],2)+h[5]*h[5];
			double t1 = pow(1.0-h[5],2)*exp(-h[3]*(rij-h[2]))/den;
			double t2 = h[5]*h[5]*exp(-h[4]*(rij-h[2]))/den;
			double der = 2*h[1]*(1.0-t1-t2)*(h[3]*t1+h[4]*t2);
			for(int d=0;d<3;d++)
			{
				FO(i,d) += -der*rijdiff(d)/rij;
				FH(j,d) -= -der*rijdiff(d)/rij;
			}
		}
		
	}
}

void forces3B()
{
	for(int i=0;i<nO;i++)
		for(int j=0;j<nH;j++)
			for(int kk=0;kk<j;kk++)
			{
				arma::rowvec rijdiff;
				double rij = diff(rH.row(j),rO.row(i),rijdiff);
				arma::rowvec rikdiff;
				double rik = diff(rH.row(kk),rO.row(i),rikdiff);
				double dtp = dot(rijdiff,rikdiff)/(rij*rik);
				if(dtp>1) dtp=1;
				if(dtp<-1) dtp=-1;
				
				double theta = acos(dtp);
				
				double dr1 = rij - r0;
				double dr2 = rik - r0;
				double dtheta = theta - theta0;
				
				
				double fcut = fcutoff(rij,rik,theta);
				if(fcut<1E-16)
					continue;
					
				double dfdr1 = -2*fcut*(m[1]*dr1 + m[3]*dr1*dtheta*dtheta);
				double dfdr2 = -2*fcut*(m[1]*dr2 + m[3]*dr2*dtheta*dtheta);
				double dfdtheta = -2*fcut*(m[2]*dtheta + m[3]*dtheta*(dr1*dr1+dr2*dr2));
				
				double dvdr1 = k[2] + 2*k[4]*dr1 + k[5]*dr2 + k[7]*dtheta + 3*k[8]*dr1*dr1;
				dvdr1 += k[9]*(2*dr1*dr2+dr2*dr2) + 2*k[11]*dr1*dtheta + k[12]*dr2*dtheta + k[13]*dtheta*dtheta;
				dvdr1 += 4*k[14]*dr1*dr1*dr1 + 2*k[15]*dr1*dr2*dr2;
				
				
				double dvdr2 = k[2] + 2*k[4]*dr2 + k[5]*dr1 + k[7]*dtheta + 3*k[8]*dr2*dr2;
				dvdr2 += k[9]*(2*dr1*dr2+dr1*dr1) + 2*k[11]*dr2*dtheta + k[12]*dr1*dtheta + k[13]*dtheta*dtheta;
				dvdr2 += 4*k[14]*dr2*dr2*dr2 + 2*k[15]*dr1*dr1*dr2;
				
				double dvdtheta = k[3] + 2*k[6]*dtheta + k[7]*(dr1+dr2) + 3*k[10]*dtheta*dtheta;
				dvdtheta += k[11]*(dr1*dr1+dr2*dr2) + k[12]*dr1*dr2 + 2*k[13]*dtheta*(dr1+dr2);
				dvdtheta += 4*k[16]*dtheta*dtheta*dtheta;
				
				
				double v0=k[1] + k[2]*(dr1+dr2) + k[3]*dtheta;
				v0 += k[4]*(dr1*dr1+dr2*dr2)+k[5]*dr1*dr2+k[6]*dtheta*dtheta+k[7]*(dr1+dr2)*dtheta;
				v0 += k[8]*(dr1*dr1*dr1+dr2*dr2*dr2) + k[9]*(dr1*dr1*dr2+dr1*dr2*dr2) + k[10]*pow(dtheta,3);
				v0 += k[11]*(dr1*dr1+dr2*dr2)*dtheta + k[12]*dr1*dr2*dtheta + k[13]*(dr1+dr2)*dtheta*dtheta;
				v0 += k[14]*(pow(dr1,4)+pow(dr2,4)) + k[15]*pow(dr1*dr2,2) + k[16]*pow(dtheta,4);
				
				
				
				double part_dvdr1  = dvdr1*fcut + v0*dfdr1;
				double part_dvdr2  = dvdr2*fcut + v0*dfdr2;
				double part_dtheta = dvdtheta*fcut + v0*dfdtheta;
				
				
				
				
				
				double cfac = 0;
				double sqarg = 1.0 - dtp*dtp;
				if(fabs(sqarg)>1E-16)
					cfac = -1.0/sqrt(sqarg);
				
				for(int d=0;d<3;d++)
				{
					double dr1dvec = rijdiff(d)/rij;
					double dr2dvec = rikdiff(d)/rik;
					
					
					double dthetadr1vec = cfac*(rikdiff(d)/(rij*rik) - dtp*rijdiff(d)/(rij*rij));
					double dthetadr2vec = cfac*(rijdiff(d)/(rij*rik) - dtp*rikdiff(d)/(rik*rik));
					
					FH(j,d) += part_dvdr1*dr1dvec + part_dtheta*dthetadr1vec;
					FO(i,d) -= part_dvdr1*dr1dvec + part_dtheta*dthetadr1vec;
					FH(kk,d) += part_dvdr2*dr2dvec + part_dtheta*dthetadr2vec;
					FO(i,d) -= part_dvdr2*dr2dvec + part_dtheta*dthetadr2vec;
				}
				
				
			}
	
}

void getdmudriO(int i, int d, arma::vec& muder)
{
	arma::vec derE;
	arma::mat derD;
	
	derE.zeros(3*nO);
	derD.zeros(3*nO,3*nO);
	
	
	for(int j=0;j<nO;j++)
	{
				
		if(j==i)
			continue;
			
		arma::rowvec rijdiff;
		double dij = diff(rO.row(i),rO.row(j),rijdiff)/a0;
		double dij2 = pow(dij,2);
		double dij3 = dij2*dij;
		double dij4 = dij3*dij;
		double dij5 = dij4*dij;
		
		double coff=Soocd(dij);
		double dcoff=dSoocd(dij);
		
		for(int k=0;k<3;k++)
		{
			double t1 = dcoff*rijdiff(k)*rijdiff(d)/(dij4*a0*a0);
			double t2 = 3*coff*rijdiff(k)*rijdiff(d)/(dij5*a0*a0);
			double t3=0;
			if(k==d)
				t3 = coff/dij3;
			
			double val = -qO*(t1 - t2 + t3);
			
			derE(j+k*nO) += val;
			derE(i+k*nO) -= val;
			
		}
		
	}
	
	for(int j=0;j<nH;j++)
	{
		arma::rowvec rijdiff;
		double dij = diff(rH.row(j),rO.row(i),rijdiff)/a0;
		double dij2 = dij*dij;
		double dij3 = dij2*dij;
		double dij4 = dij3*dij;
		double dij5 = dij4*dij;
		
			
		double coff=Sohcd(dij);
		double dcoff=dSohcd(dij);
			
		for(int k=0;k<3;k++)
		{
			double t1 = dcoff*rijdiff(k)*rijdiff(d)/(dij4*a0*a0);
			double t2 = 3*coff*rijdiff(k)*rijdiff(d)/(dij5*a0*a0);
			
			double t3=0;
			if(k==d)
				t3 = coff/dij3;
				
			double val = -t1+t2-t3;	
			derE(i+k*nO) += -qH*val;
		}	
			
			
	}
	
	
	
	// now Dcoupling derivative
	for(int n2=0;n2<nO;n2++)
	{
		if(n2==i)
			continue;
			
		arma::rowvec rijdiff;
		double dij = diff(rO.row(n2),rO.row(i),rijdiff)/a0;
		//double dij3 = pow(dij,3);
		double dij4 = pow(dij,4);
		double dij5 = pow(dij,5);
		double dij6 = pow(dij,6);
		double dij7 = pow(dij,7);
		double coff = Soodd(dij);
		double dcoff = dSoodd(dij);
		for(int d1=0;d1<3;d1++)
		{
			for(int d2=0;d2<3;d2++)
			{
				double t1 = 3*dcoff*rijdiff(d1)*rijdiff(d2)*rijdiff(d)/(dij6*a0*a0*a0);
				double t2 = -15*coff*rijdiff(d1)*rijdiff(d2)*rijdiff(d)/(dij7*a0*a0*a0);
				double tdeltas=0;
				if(d==d1)
					tdeltas += rijdiff(d2)/a0;
				if(d==d2)
					tdeltas += rijdiff(d1)/a0;
				
				tdeltas *= 3.0*coff/dij5;
				
					
				double val = t1 + t2 + tdeltas;
				derD(i+d1*nO,n2+d2*nO) += -val;
				derD(n2+d1*nO,i+d2*nO) += -val;
			}
			double val = dcoff*rijdiff(d)/(dij4*a0) - 3*coff*rijdiff(d)/(dij5*a0);
			derD(i+d1*nO,n2+d1*nO) += val;
			derD(n2+d1*nO,i+d1*nO) += val;
			
		}
	}
	
	arma::vec md = alpha*Dinverse*(derE+derD*mustrip);
	muder.zeros(3*nO);
	for(int i=0;i<3*nO;i++)
		muder(i)=md(i)/a0;    // to have the derivative in angstrom
	
}


void getdmudriH(int k, int d, arma::vec& muder)
{
	arma::vec derE;
	
	derE.zeros(3*nO);
	
	
	for(int i=0;i<nO;i++)
	{
		arma::rowvec rijdiff;
		double dij = diff(rH.row(k),rO.row(i),rijdiff)/a0;
		double dij2 = dij*dij;
		double dij3 = dij2*dij;
		double dij4 = dij3*dij;
		double dij5 = dij4*dij;
		
			
		double coff=Sohcd(dij);
		double dcoff=dSohcd(dij);
			
		for(int d1=0;d1<3;d1++)
		{
			double t1 = dcoff*rijdiff(d1)*rijdiff(d)/(dij4*a0*a0);
			double t2 = -3*coff*rijdiff(d1)*rijdiff(d)/(dij5*a0*a0);
			
			double t3=0;
			if(d==d1)
				t3 = coff/dij3;
				
			double val = t1+t2+t3;	
			derE(i+d1*nO) += -qH*val;
		}	
			
			
	}
	
	
	
	// Dcoupling derivative vs rH is 0
	
	
	arma::vec md = alpha*Dinverse*derE;
	muder.zeros(3*nO);
	for(int i=0;i<3*nO;i++)
		muder(i)=md(i)/a0;    // to have the derivative in angstrom
	
}

double dgdipole(double muksquare){

	// NOTE: this the derivative over musquare, that is argument
	double den = muksquare + mu0*mu0 + dpar*muksquare*muksquare;
	double t1 = 1.0/den;
	double t2 = (muksquare-mu0*mu0)*(1.0 + 2*dpar*muksquare)/(den*den);
	return t1-t2;
}
		

void forcesOSS3()
{
	
	
	double V[nO];  // needed for the dipole moment derivative
	
	for(int i=0;i<nO;i++)
	{	
		V[i]=0;
		
		
		
		for(int k1=0;k1<nH;k1++)
			for(int k2=0;k2<k1;k2++)
			{
				
				arma::rowvec rik1diff;
				double r1 = diff(rH.row(k1),rO.row(i),rik1diff);
				double dr1 = r1-r0;
				arma::rowvec rik2diff;
				double r2 = diff(rH.row(k2),rO.row(i),rik2diff);
				double dr2 = r2-r0;
					
				double dtp = dot(rik1diff,rik2diff);
				double tfac = dtp/(r1*r2);
				if(tfac>1) tfac=1;
				if(tfac<-1) tfac=-1;
				
				double theta = acos(tfac);
				double dtheta = theta - theta0;
					
				double coff = fcutoff(r1,r2,theta);
				
				
				double tacc = p[1] + p[2]*dtheta + p[3]*dr1*dr2 + p[4]*dtheta*dtheta + p[5]*(dr1+dr2)*dtheta;
				tacc += p[6]*(dr1*dr1*dr2 + dr1*dr2*dr2) + p[7]*(dr1*dr1+dr2*dr2)*dtheta + p[8]*dr1*dr2*dtheta;
				tacc += p[9]*(dr1+dr2)*dtheta*dtheta + p[10]*pow(dtheta,3) + p[11]*pow(dr1*dr2,2) + p[12]*pow(dtheta,4);
				V[i] += tacc*coff;
		
			}
	}
		
	
	// this should get us only the O vs O part
	for(int i=0;i<nO;i++)
	{
		arma::vec dmux,dmuy,dmuz;
		
		getdmudriO(i,0,dmux);
		getdmudriO(i,1,dmuy);
		getdmudriO(i,2,dmuz);
		
		for(int j=0;j<nO;j++)
		{
			double musquare = dot(muvec.row(j),muvec.row(j));
			double dgmucut = dgdipole(musquare)*V[j];
			FO(i,0) += 2*dgmucut*(dmux(j)*muvec(j,0)+dmux(j+nO)*muvec(j,1)+dmux(j+2*nO)*muvec(j,2));
			FO(i,1) += 2*dgmucut*(dmuy(j)*muvec(j,0)+dmuy(j+nO)*muvec(j,1)+dmuy(j+2*nO)*muvec(j,2));
			FO(i,2) += 2*dgmucut*(dmuz(j)*muvec(j,0)+dmuz(j+nO)*muvec(j,1)+dmuz(j+2*nO)*muvec(j,2));
		}
		
	}

	// we do the same for the H part
	for(int i=0;i<nH;i++)
	{
		arma::vec dmux,dmuy,dmuz;
		
		getdmudriH(i,0,dmux);
		getdmudriH(i,1,dmuy);
		getdmudriH(i,2,dmuz);
		
		for(int j=0;j<nO;j++)
		{
			double musquare = dot(muvec.row(j),muvec.row(j));
			double dgmucut = dgdipole(musquare)*V[j];
			FH(i,0) += 2*dgmucut*(dmux(j)*muvec(j,0)+dmux(j+nO)*muvec(j,1)+dmux(j+2*nO)*muvec(j,2));
			FH(i,1) += 2*dgmucut*(dmuy(j)*muvec(j,0)+dmuy(j+nO)*muvec(j,1)+dmuy(j+2*nO)*muvec(j,2));
			FH(i,2) += 2*dgmucut*(dmuz(j)*muvec(j,0)+dmuz(j+nO)*muvec(j,1)+dmuz(j+2*nO)*muvec(j,2));
		}
		
	}

	
	
	
	
	
	// coordinates part
	
	for(int i=0;i<nO;i++)
	{
		double musquare = dot(muvec.row(i),muvec.row(i));
		double gmucut = gdipole(musquare);
		
		for(int j=0;j<nH;j++)
			for(int kk=0;kk<j;kk++)
			{
				arma::rowvec rijdiff;
				double rij = diff(rH.row(j),rO.row(i),rijdiff);
				arma::rowvec rikdiff;
				double rik = diff(rH.row(kk),rO.row(i),rikdiff);
				double dtp = dot(rijdiff,rikdiff)/(rij*rik);
				if(dtp>1) dtp=1;
				if(dtp<-1) dtp=-1;
				
				double theta = acos(dtp);
				
				double dr1 = rij - r0;
				double dr2 = rik - r0;
				double dtheta = theta - theta0;
				
				
				double fcut = fcutoff(rij,rik,theta);
				
				double dfdr1 = -2*fcut*(m[1]*dr1 + m[3]*dr1*dtheta*dtheta);
				double dfdr2 = -2*fcut*(m[1]*dr2 + m[3]*dr2*dtheta*dtheta);
				double dfdtheta = -2*fcut*(m[2]*dtheta + m[3]*dtheta*(dr1*dr1+dr2*dr2));
				
				double dvdr1 = p[3]*dr2 + p[5]*dtheta + p[6]*(2*dr1*dr2 + dr2*dr2) + p[7]*2*dr1*dtheta;
				dvdr1 += p[8]*dr2*dtheta + p[9]*dtheta*dtheta + p[11]*2*dr1*dr2*dr2;
				
				
				double dvdr2 = p[3]*dr1 + p[5]*dtheta + p[6]*(2*dr1*dr2 + dr1*dr1) + p[7]*2*dr2*dtheta;
				dvdr2 += p[8]*dr1*dtheta + p[9]*dtheta*dtheta + p[11]*2*dr2*dr1*dr1;
				
				double dvdtheta = p[2] + 2*p[4]*dtheta + p[5]*(dr1+dr2) + p[7]*(dr1*dr1+dr2*dr2);
				dvdtheta += p[8]*dr1*dr2 + p[9]*2*dtheta*(dr1+dr2) + 3*p[10]*dtheta*dtheta + 4*p[12]*dtheta*dtheta*dtheta;
				
				
				double v0= p[1] + p[2]*dtheta + p[3]*dr1*dr2 + p[4]*dtheta*dtheta + p[5]*(dr1+dr2)*dtheta;
				v0 += p[6]*(dr1*dr1*dr2 + dr1*dr2*dr2) + p[7]*(dr1*dr1+dr2*dr2)*dtheta + p[8]*dr1*dr2*dtheta;
				v0 += p[9]*(dr1+dr2)*dtheta*dtheta + p[10]*pow(dtheta,3) + p[11]*pow(dr1*dr2,2) + p[12]*pow(dtheta,4);
				
				
				double part_dvdr1  = dvdr1*fcut + v0*dfdr1;
				double part_dvdr2  = dvdr2*fcut + v0*dfdr2;
				double part_dtheta = dvdtheta*fcut + v0*dfdtheta;
				
				
				
				double cfac = 0;
				
				double sqarg = 1.0 - dtp*dtp;
				if(fabs(sqarg)>1E-16)
					cfac = -1.0/sqrt(sqarg);
				
				
				for(int d=0;d<3;d++)
				{
					double dr1dvec = rijdiff(d)/rij;
					double dr2dvec = rikdiff(d)/rik;
					
					
					double dthetadr1vec = cfac*(rikdiff(d)/(rij*rik) - dtp*rijdiff(d)/(rij*rij));
					double dthetadr2vec = cfac*(rijdiff(d)/(rij*rik) - dtp*rikdiff(d)/(rik*rik));
					
					FH(j,d) += gmucut*(part_dvdr1*dr1dvec + part_dtheta*dthetadr1vec);
					FO(i,d) -= gmucut*(part_dvdr1*dr1dvec + part_dtheta*dthetadr1vec);
					FH(kk,d) += gmucut*(part_dvdr2*dr2dvec + part_dtheta*dthetadr2vec);
					FO(i,d) -= gmucut*(part_dvdr2*dr2dvec + part_dtheta*dthetadr2vec);
				}
				
				
			}
	}
	
	
} 



};

#endif
