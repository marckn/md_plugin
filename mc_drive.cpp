#include "mc_drive.h"

namespace plug_mcdrive
{


	extern "C" void loadData(int rank, const std::string& fname, bool _verbose)
	{
		if(rank==0)
			std::cout<<"############ Plug-in: mc_drive ############"<<std::endl;
		
		verbose=_verbose;
		mpirank = rank;
		std::string fstr("MCDRIVE Rank " + std::to_string(mpirank));
		std::seed_seq seed(fstr.begin(),fstr.end());
		rgen.seed(seed);
		dist = std::uniform_real_distribution<double>(0,1);
		ndist = std::normal_distribution<double>(0,1);
		
		pt::ptree tree;
		pt::read_xml(fname,tree,pt::xml_parser::no_comments|pt::xml_parser::trim_whitespace);
	
		dimensions = tree.get<int>("dimensions");
		T = tree.get<double>("temperature");
		
		pimd_beads = tree.get<int>("pimd_beads",1);
		
		length=1E-10;
		mass = 1.660539040E-27;
		energy = 1.60217662E-19;
		kb = 1.38064852E-23 / energy;
		time = 1E-12;
		hbar = 1.0545718E-34 / (energy*time);
		beta = 1.0/(kb*T);
		charge = 1.60217662E-19;
		spring_coeff = 0.5*pimd_beads/(hbar*hbar*beta);
		
		epsilon0 = 8.8541878E-12*energy*length/(charge*charge);	
		coulfac=4*acos(-1)*epsilon0;
		econv_to_md = energy * pow(time/length,2) / mass; // energy unit converted in length-mass-time units
		std::vector<std::vector<double> > cfg;
		
		int sknt=0;
		for(auto rval : tree.get_child("system"))
		{
			if(rval.first != "subs")
				continue;
				
			std::string cnode = rval.first;
			std::string ptype = rval.second.get<std::string>("<xmlattr>.potential");
			std::string ptag = rval.second.get<std::string>("<xmlattr>.tag");
			int iid = rval.second.get("coupled",-1);
			if(iid!=-1)
			{
				coupled_sub = sknt;
				coupled_idx = iid;
			}
			sknt++;
			if(cnode == "subs"){
				if(ptype=="oss3")
					loadOSSSubs(rval.second,cfg);
				else if(ptype=="charmm")
					loadCharmmSubs(rval.second,cfg);
				else if(ptype=="lj")
					loadLJSubs(rval.second,cfg);
				else
					throw std::runtime_error("unsupported potential type");
				
				
			}
		}
		
		for(auto rval : tree.get_child("system"))
		{
			if(rval.first != "intersub_interaction")
				continue;
				
			std::string tag1 = rval.second.get<std::string>("<xmlattr>.sub1");
			std::string tag2 = rval.second.get<std::string>("<xmlattr>.sub2");
			
			loadInterSub(tag1,tag2,rval.second);
			
		}
		
		
		
		
		
		
		N = cfg.size();
		initial_config.zeros(N,2*dimensions);
		
		for(int i=0;i<N;i++)
			for(int d=0;d<dimensions;d++)
			{
				initial_config(i,d) = cfg[i][d];
				initial_config(i,d+dimensions) = ndist(rgen) * sqrt(masses[i]*econv_to_md/beta);
				
			}
		
		for(int ii : freeze_list)
				for(int d=0;d<dimensions;d++)
					initial_config(ii,d+dimensions)=0;
		
		current_config = initial_config;
		
		
		if(boost::optional<std::string> fnm = tree.get_optional<std::string>("MCSampling"))
		{
			useMC=true;
			MCS = tree.get<int>("MCSampling.steps");
			MRT_delta = tree.get<double>("MCSampling.MRT_delta");
			if(mpirank==0)
			{
				std::cout<<" ## Doing MC Sampling with:"<<std::endl;
				std::cout<<"\t ** MRT delta             = "<<MRT_delta<<std::endl;
				std::cout<<"\t ** steps                 = "<<MCS<<std::endl;
			}
		}
		else{
			useMD=true;
			md_dt = tree.get<double>("MDSampling.dt");
			md_gammath = tree.get<double>("MDSampling.thermos_friction");
			md_nsteps = tree.get<int>("MDSampling.steps");
			md_equil = tree.get<int>("MDSampling.equilibration",0);
			if(mpirank==0)
			{
				std::cout<<" ## Doing MD Sampling with:"<<std::endl;
				std::cout<<"\t ** dt                    = "<<md_dt<<std::endl;
				std::cout<<"\t ** thermostat friction   = "<<md_gammath<<std::endl;
				std::cout<<"\t ** steps                 = "<<md_nsteps<<std::endl;
				std::cout<<"\t ** equilibration steps   = "<<md_equil<<std::endl;
				
			}
		
			
		}
		
		
		if(mpirank==0){
			std::cout<<"###### Subsystems map ######"<<std::endl;
			for(Subs s : subsystems)
			{
				std::cout<<"\t ** Subsystem     : "<<s.tag<<std::endl;
				std::cout<<"\t ** Particles     : "<<s.Ntot<<std::endl;
				std::cout<<"\t ** Interaction   : "<<s.potential_type<<std::endl;
				std::cout<<"\t *************************************"<<std::endl<<std::endl;	
			}
			
			
			std::cout<<"###### Subsystems interactions ######"<<std::endl;
			for(ISubs s : intersub)
			{
				std::cout<<"\t ** Found between   : "<<s.tag1<<" and "<<s.tag2<<std::endl;
				std::cout<<"\t ** Type:           : "<<s.type1<<", "<<s.type2<<std::endl;
				std::cout<<"\t *************************************"<<std::endl<<std::endl;
			}
			
			std::cout<<"## Loaded "<<N<<" particles from input"<<std::endl;
			std::cout<<"############################################"<<std::endl;
		
		
			std::cout<<"###### Restraints map ######"<<std::endl;
			for(Rest r : restraints)
				std::cout<<"\t ** FOUND (i1,i2) = ("<<r.i1<<","<<r.i2<<")"<<std::endl;
			for(Rest r : sampling_restraints)
				std::cout<<"\t ** FOUND (i1,i2) = ("<<r.i1<<","<<r.i2<<") - Sampling only"<<std::endl;
			
			if(referenceRestraints.size()>0)
				std::cout<<"\t ** FOUND "<<referenceRestraints.size()<<" REFERENCE RESTRAINTS"<<std::endl;
				
			std::cout<<"############################################"<<std::endl;
		        
			
			
			std::cout<<"###### Frozen particles ######"<<std::endl;
			for(int i : freeze_list)
				std::cout<<"\t ** FOUND idx = "<<i<<std::endl;
			std::cout<<"############################################"<<std::endl;
		
			
		}
		
		if(boost::optional<std::string> fnm = tree.get_optional<std::string>("restart"))
		{
			std::string dpath = tree.get<std::string>("restart");
			restart(dpath);
		}
		
		equilibrate();	
		// debug function
		//gogogo();
		
		
	
	}
	
	
	void restart(std::string dirpath)
	{
		std::string xcfile = dirpath+"/nd."+std::to_string(mpirank)+".x.xyz";
		std::string pcfile = dirpath+"/nd."+std::to_string(mpirank)+".p.xyz";
		
		std::ifstream inpx(xcfile);
		while(!inpx.eof())
		{
			std::string cl;
			std::getline(inpx,cl);
			std::stringstream sscl1(cl);
			int cfn;
			sscl1>>cfn;
			
			if(cfn != N)
				std::runtime_error("Input restart configuration does not match the system size");
			
			std::getline(inpx,cl);
			if(inpx.eof())
				break;
				
			for(int i=0;i<N;i++)
			{
				std::string cdt;
				std::getline(inpx,cdt);
				std::stringstream sscdt(cdt);
				std::string tag;
				double x,y,z;
				sscdt>>tag>>x>>y>>z;
				current_config(i,0) = x;
				current_config(i,1) = y;
				current_config(i,2) = z;
			}
		}
		
		std::ifstream inpy(pcfile);
		while(!inpy.eof())
		{
			std::string cl;
			std::getline(inpy,cl);
			std::stringstream sscl1(cl);
			int cfn;
			sscl1>>cfn;
			
			if(cfn != N)
				std::runtime_error("Input restart configuration does not match the system size");
			
			std::getline(inpy,cl);
			
			if(inpy.eof())
				break;
			
			for(int i=0;i<N;i++)
			{
				std::string cdt;
				std::getline(inpy,cdt);
				std::stringstream sscdt(cdt);
				std::string tag;
				double x,y,z;
				sscdt>>tag>>x>>y>>z;
				current_config(i,3) = x;
				current_config(i,4) = y;
				current_config(i,5) = z;
			}
		}
		
		
	}
	
	
	extern "C" int getSolventN(){ return N;}
	
	extern "C" arma::vec getMasses(){return arma::vec(masses);}
	
	
	extern "C" arma::mat Sample()
	{
		if(useMC)
			return MCSample();
		else
			return MDSample();
			
	}
	extern "C" arma::mat Forces(const arma::mat& config)
	{
		arma::mat f(N,dimensions,arma::fill::zeros);
		
		for(Subs& s : subsystems)
		{
			if(s.potential_type == "oss3")
				f += ossForces(s,config);
			if(s.potential_type == "charmm")
				f += charmmForces(s,config);
			
			// add more potential types here
		}
		
		for(ISubs& s : intersub)
			f += intersubForces(s,config);
		
		for(Rest& r : restraints)
			f += restForces(r,config);
		
		
		for(RefRest& r : referenceRestraints)
			f += refRestForces(r,config);
		
		
		for(int ii : freeze_list)
			for(int d=0;d<dimensions;d++)
				f(ii,d)=0;
			
		
		return f;
	}

	arma::mat ForcesMD(const arma::mat& config)
	{
		arma::mat f(N,dimensions,arma::fill::zeros);
		
		for(Subs& s : subsystems)
		{
			if(s.potential_type == "oss3")
				f += ossForces(s,config);
			if(s.potential_type == "charmm")
				f += charmmForces(s,config);
			
			// add more potential types here
		}
		
		for(ISubs& s : intersub)
			f += intersubForces(s,config);
		
		for(Rest& r : restraints)
			f += restForces(r,config);
		
		
		for(Rest& r : sampling_restraints)
			f += restForces(r,config);
		
		
		for(RefRest& r : referenceRestraints)
			f += refRestForces(r,config);
		
		
		for(int ii : freeze_list)
			for(int d=0;d<dimensions;d++)
				f(ii,d)=0;
			
		
		return f;
	}


	extern "C" double totalEnergy(const arma::mat& config)
	{
		double en=0;
		
		
		for(Subs& s : subsystems)
		{
			if(s.potential_type == "oss3")
				en += ossEnergy(s,config);
			if(s.potential_type == "charmm")
				en += charmmEnergy(s,config);
			
			// add more potential types here
		}
		
		for(ISubs& s : intersub)
			en += intersubEnergy(s,config);
		
		for(Rest& r : restraints)
			en += restEnergy(r,config);
			
		for(RefRest& r : referenceRestraints)
			en += refRestEnergy(r,config);
			
		return en;
	}
	
	extern "C" double atomEnergy(const arma::mat& config,int nsolv)
	{
		
		std::string ctag = subsystems[coupled_sub].tag;
		std::string ctype = subsystems[coupled_sub].potential_type;
		
		double en=0;
		
		if(ctype == "oss3")
		{
			int cidx = subsystems[coupled_sub].nall_to_nonh[coupled_idx];
			ossEnergy(subsystems[coupled_sub], config);
			double hatoev = 27.2114;
			en += subsystems[coupled_sub].p_oss.en_atom(cidx)*hatoev;
		}
		if(ctype == "charmm")
		{
			std::cout<<"Charmm energy per atom not yet implemented"<<std::endl;
			en += 0;//charmmEnergyAtom(subsystems[isub],config,coupled_idx);
		}
		
		for( ISubs& s : intersub)
		{
			
			std::vector<double>* epslj;
			std::vector<double>* siglj;
			std::vector<int>*    stocfg;
			double epsilon,sigma;
			
			if(s.tag1 == ctag)
			{
				epsilon = s.ljepsilon_s1[coupled_idx];
				sigma = s.ljsigma_s1[coupled_idx];
				epslj = &s.ljepsilon_s2;
				siglj = &s.ljsigma_s2;
				stocfg = &s.s2tocfg;
			}else{
				epsilon = s.ljepsilon_s2[coupled_idx];
				sigma = s.ljsigma_s2[coupled_idx];
				epslj = &s.ljepsilon_s1;
				siglj = &s.ljsigma_s1;
				stocfg = &s.s1tocfg;
			}
			
			for(unsigned int j=0;j<stocfg->size();j++)
			{
				double elj = sqrt(epsilon * (*epslj)[j]);
				double slj = 0.5*(sigma + (*siglj)[j]);
				double dquad = 0;
				for(int d=0;d<dimensions;d++)
					dquad += pow(config(nsolv,d)-config((*stocfg)[j],d),2);
		
				
				double dist = sqrt(dquad);
				double fac = pow(slj/dist,6);
				double vlj = 4*elj*fac*(fac-1);
				
				// coulomb part
				double q1 = charges[nsolv];
				double q2 = charges[(*stocfg)[j]];
				double vcoul = q1*q2/(dist*coulfac);
				
				en += vlj+vcoul;
			}
			
			
			
			
			
			
		}
		return en;
		
	}
	
	
	// plugin internal functions
	std::vector<std::pair<std::string, std::vector<double> > > loadXYZfile(std::string& fname)
	{
		std::ifstream inp(fname);
		if(!inp)
			throw std::runtime_error("XYZ file not found");
			
		std::vector<std::pair<std::string, std::vector<double> > > rval;
		std::string line;
		int nlines;
		std::getline(inp,line);
		std::stringstream sti(line);
		sti>>nlines;
		
		std::string header;
		std::getline(inp,header);
		for(int i=0;i<nlines;i++)
		{
			std::string tag;
			std::vector<double> coords;
			double x,y,z;
			std::getline(inp,line);
			std::stringstream stv(line);
			
			stv>>tag>>x>>y>>z;
			coords.push_back(x);
			coords.push_back(y);
			coords.push_back(z);
			rval.push_back(std::pair<std::string,std::vector<double> >(tag,coords));
		}
		
		return rval;
			
	}
	
	
	std::vector<std::pair<std::string, std::vector<double> > > loadConfig(pt::ptree& inp)
	{
		std::vector<std::pair<std::string, std::vector<double> > > retval;
		for(auto rval : inp.get_child("initial_configuration"))
		{
			std::string ctag = rval.first;
			
			std::string cvals = rval.second.get<std::string>("");
			std::stringstream sst(cvals);
			std::vector<double> values;
			for(int d=0;d<dimensions;d++)
			{
				double v;
				sst>>v;
				values.push_back(v);
			}
			
			retval.push_back(std::pair<std::string,std::vector<double> >(ctag,values));
		}
		return retval;
	}
	
	
	void loadOSSSubs(pt::ptree& inp, std::vector<std::vector<double> >& cfg)
	{
		if(mpirank==0)
			std::cout<<"\t == Initializing OSSv3 potential"<<std::endl;
				
		std::vector<std::pair<std::string, std::vector<double> > > data;
		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("xyz"))
			data = loadXYZfile(*fnm);
		else
			data = loadConfig(inp);
		
		
		
		std::map<std::string,double> tm;
		std::map<std::string,double> tc;
		
		for(auto rval : inp.get_child("masses"))
			tm[rval.first]=rval.second.get<double>("");
		
		
		tc["O"] = inp.get<double>("charges.O",0);
		tc["H"] = inp.get<double>("charges.H",0);
		
		Subs s;
		s.potential_type="oss3";
		s.tag = inp.get<std::string>("<xmlattr>.tag");
		
		int cidx = cfg.size();

		for(auto v : data)
		{
			
			std::string ctag = v.first;
			std::vector<double> coords = v.second;
			if(ctag == "O")
				s.nO_to_cfg.push_back(cidx);
			if(ctag == "H")
				s.nH_to_cfg.push_back(cidx);
			
			s.nall_to_cfg.push_back(cidx);
			cidx++;
			cfg.push_back(coords);	
			tags.push_back(ctag);
			masses.push_back(tm[ctag]);
			charges.push_back(tc[ctag]);
			
			
		}

		// reverse pointer for no+nh vector
		for(int cid : s.nall_to_cfg)
		{
			int knt=0;
			bool found=false;
			for(int cno : s.nO_to_cfg)
			{
				if(cno == cid)
				{
					s.nall_to_nonh.push_back(knt);
					found=true;
					break;
				}
				knt++;
			}
			if(found)
				continue;
			
			for(int cnh : s.nH_to_cfg)
			{
				if(cnh == cid)
				{
					s.nall_to_nonh.push_back(knt);
					break;
				}
				knt++;
			}
		}
		
		
		int _nO = s.nO_to_cfg.size();
		int _nH = s.nH_to_cfg.size();
			
		s.oss_nO.zeros(_nO,dimensions);
		s.oss_nH.zeros(_nH,dimensions);
		s.p_oss.init(_nO,_nH,"OSS3",false);
		
		s.Ntot = _nO + _nH;
		
		
		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("restraints"))
			for(auto rval : inp.get_child("restraints"))
			{
				Rest r;
				std::string type=rval.second.get<std::string>("<xmlattr>.type","none");
				r.d = rval.second.get<double>("<xmlattr>.d");
				r.k = rval.second.get<double>("<xmlattr>.k");
				r.nexp = rval.second.get<int>("<xmlattr>.n");
			
				std::string vals = rval.second.get<std::string>("");
				std::stringstream svl(vals);
				int i1sub,i2sub;
				svl>>i1sub>>i2sub;
				
				r.i1=s.nall_to_cfg[i1sub];
				r.i2=s.nall_to_cfg[i2sub];
				if(type=="sampling" || type=="SAMPLING" || type=="Sampling")
					sampling_restraints.push_back(r);
				else
					restraints.push_back(r);
			}
		
		
		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("referenceRestraints"))
			for(auto rval : inp.get_child("referenceRestraints"))
			{
				RefRest r;
				r.k = rval.second.get<double>("<xmlattr>.k");
				r.nexp = rval.second.get<int>("<xmlattr>.n");
				int cidx = rval.second.get<int>("<xmlattr>.idx");
				r.idx = s.nall_to_cfg[cidx];
				std::string vals = rval.second.get<std::string>("");
				std::stringstream svl(vals);
				double x,y,z;
				svl>>x>>y>>z;
				r.ref<<x<<arma::endr<<y<<arma::endr<<z;
				referenceRestraints.push_back(r);
			}

		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("freeze"))
		{
			std::stringstream frs(*fnm);
			while(!frs.eof())
			{
				int il;
				frs>>il;
				freeze_list.push_back(s.nall_to_cfg[il]);
			}
		}
		
		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("pimd_list"))
		{
			std::stringstream frs(*fnm);
			while(!frs.eof())
			{
				int il;
				frs>>il;
				pimd_list.push_back(s.nall_to_cfg[il]);
			}
		}
	
	
		
		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("virtual_sites"))
		{
			for(auto rval : inp.get_child("virtual_sites"))
			{
				VSite vs;
				vs.a = rval.second.get<double>("<xmlattr>.a");
				vs.b = rval.second.get<double>("<xmlattr>.b");
				std::string sls = rval.second.get<std::string>("");
				std::stringstream sin(sls);
				sin>>vs.i1>>vs.i2>>vs.isite;		
				vs.i1= s.nall_to_cfg[vs.i1];
				vs.i2= s.nall_to_cfg[vs.i2];
				vs.isite= s.nall_to_cfg[vs.isite];
				vsites.push_back(vs);
				freeze_list.push_back(vs.isite);		
			}
		}
	
			

	
		
		
		subsystems.push_back(s);
	}
	
	void loadCharmmSubs(pt::ptree& inp, std::vector<std::vector<double> >& cfg)
	{
		if(mpirank==0)
			std::cout<<"\t == Initializing the CHARMM forcefield"<<std::endl;
				
		std::vector<std::pair<std::string, std::vector<double> > > data;
		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("xyz"))
			data = loadXYZfile(*fnm);
		else
			data = loadConfig(inp);
		
		
		Subs s;
		s.potential_type="charmm";
		s.tag = inp.get<std::string>("<xmlattr>.tag");
		s.charmmdir = inp.get<std::string>("charmmdir");
		s.topology = inp.get<std::string>("topology");
		s.verlet_r = inp.get<double>("verlet_radius",99999);
		s.cutoff_r = inp.get<double>("cutoff_radius",99999);
		
		if(s.cutoff_r > s.verlet_r)
		{
			std::cout<<"!!! CHARMM FF WARNING: your cutoff radius is larger than the verlet radius. Setting them to the biggest value"<<std::endl;
			s.cutoff_r = s.verlet_r;
		}
		
		double Lbox[3]={1000,1000,1000}; // not really used as long as pbc are not implemented
		
		double kjmoltoev = 0.010364305332435094;
		double fconv = kjmoltoev*0.1;
		
		bool silent=true;
		if(mpirank==0)
			silent = !verbose;
			
		s.charmm_ff.init(Lbox,false,s.topology,s.verlet_r,s.cutoff_r,kjmoltoev,fconv,silent,s.charmmdir);
		
		s.Ntot = data.size();
		s.charmm_buff = new double*[s.Ntot];
		s.charmm_gradients = new double*[s.Ntot];
		int cidx = cfg.size();
		for(int i=0;i<s.Ntot;i++)
		{
			std::string ctag = data[i].first;
			std::vector<double> coords = data[i].second;
			
			s.charmm_buff[i] = new double[3];
			s.charmm_gradients[i] = new double[3];
			s.charmm_buff[i][0]=s.charmm_buff[i][1]=s.charmm_buff[i][2]=0;
			s.charmm_gradients[i][0]=s.charmm_gradients[i][1]=s.charmm_gradients[i][2]=0;
			s.charmm_to_cfg.push_back(cidx+i);
			tags.push_back(ctag);
			cfg.push_back(coords);
			masses.push_back(s.charmm_ff.atomtable.atommass[i]);
			charges.push_back(s.charmm_ff.atomtable.atomcharge[i]);
			
		}
		
		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("restraints"))
			for(auto rval : inp.get_child("restraints"))
			{
				Rest r;
				r.d = rval.second.get<double>("<xmlattr>.d");
				r.k = rval.second.get<double>("<xmlattr>.k");
				r.nexp = rval.second.get<int>("<xmlattr>.n");
			
				std::string vals = rval.second.get<std::string>("");
				std::stringstream svl(vals);
				int i1sub,i2sub;
				svl>>i1sub>>i2sub;
				
				r.i1=s.charmm_to_cfg[i1sub];
				r.i2=s.charmm_to_cfg[i2sub];
				restraints.push_back(r);
			}

		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("referenceRestraints"))
			for(auto rval : inp.get_child("referenceRestraints"))
			{
				RefRest r;
				r.k = rval.second.get<double>("<xmlattr>.k");
				r.nexp = rval.second.get<int>("<xmlattr>.n");
				int cidx = rval.second.get<int>("<xmlattr>.idx");
				r.idx = s.charmm_to_cfg[cidx];
				std::string vals = rval.second.get<std::string>("");
				std::stringstream svl(vals);
				double x,y,z;
				svl>>x>>y>>z;
				r.ref<<x<<arma::endr<<y<<arma::endr<<z;
				referenceRestraints.push_back(r);
			}


		
		
		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("virtual_sites"))
		{
			for(auto rval : inp.get_child("virtual_sites"))
			{
				VSite vs;
				vs.a = rval.second.get<double>("<xmlattr>.a");
				vs.b = rval.second.get<double>("<xmlattr>.b");
				std::string sls = rval.second.get<std::string>("");
				std::stringstream sin(sls);
				sin>>vs.i1>>vs.i2>>vs.isite;		
				vs.i1= s.charmm_to_cfg[vs.i1];
				vs.i2= s.charmm_to_cfg[vs.i2];
				vs.isite= s.charmm_to_cfg[vs.isite];
				vsites.push_back(vs);
				freeze_list.push_back(vs.isite);		
			}
		}
		
		
		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("freeze"))
		{
			std::stringstream frs(*fnm);
			while(!frs.eof())
			{
				int il;
				frs>>il;
				freeze_list.push_back(s.charmm_to_cfg[il]);
			}
		}
		
		if(boost::optional<std::string> fnm = inp.get_optional<std::string>("pimd_list"))
		{
			std::stringstream frs(*fnm);
			while(!frs.eof())
			{
				int il;
				frs>>il;
				pimd_list.push_back(s.charmm_to_cfg[il]);
			}
		}
		
		
		subsystems.push_back(s);
	}

	
	void loadLJSubs(pt::ptree& inp, std::vector<std::vector<double> >& cfg)
	{
		throw std::runtime_error("LJ subsystems not yet implemented");
	}
	
	
	
	
	void loadInterSub(std::string& tag1, std::string& tag2, pt::ptree& inp)
	{
		int i1=-1,i2=-1;
		for(unsigned int i=0;i<subsystems.size();i++){
			if(subsystems[i].tag==tag1)
				i1= i;
			if(subsystems[i].tag==tag2)
				i2= i;
		}
		
		if(i1 == -1)
			throw std::runtime_error("Tag "+tag1+" not found in subsystems");
		
		if(i2 == -1)
			throw std::runtime_error("Tag "+tag2+" not found in subsystems");
		
		ISubs s;
		s.tag1=tag1;
		s.tag2=tag2;
		if(subsystems[i1].potential_type=="oss3")
			s.s1tocfg = subsystems[i1].nall_to_cfg;
		else if(subsystems[i1].potential_type=="charmm")
			s.s1tocfg = subsystems[i1].charmm_to_cfg;
		
		else
			s.s1tocfg = subsystems[i1].lj_to_cfg;
		
		
		if(subsystems[i2].potential_type=="oss3")
			s.s2tocfg = subsystems[i2].nall_to_cfg;
		else if(subsystems[i2].potential_type=="charmm")
			s.s2tocfg = subsystems[i2].charmm_to_cfg;
		else
			s.s2tocfg = subsystems[i2].lj_to_cfg;
		
		std::string tp1 = inp.get<std::string>("sub1.<xmlattr>.type");
		std::string tp2 = inp.get<std::string>("sub2.<xmlattr>.type");
		s.type1=tp1;
		s.type2=tp2;
		
		for( int i : s.s1tocfg)
			s.charge_s1.push_back(charges[i]);
				
		for( int i : s.s2tocfg)
			s.charge_s2.push_back(charges[i]);
		
		
		std::map<std::string,double> mpe, mps;
		
		double cen_unit = inp.get<double>("energy_units",energy);
		double cl_unit = inp.get<double>("length_units",length);
		
		double en_conv = cen_unit / energy;
		double l_conv = cl_unit / length;
		
		if(tp1 == "charmm")
		{
			
			for(unsigned int i=0;i<s.s1tocfg.size();i++)
			{
				double kjmoltoev = 0.010364305332435094;
				s.ljepsilon_s1.push_back(subsystems[i1].charmm_ff.atomtable.epsilon[i]*kjmoltoev); // convert to eV
				s.ljsigma_s1.push_back(subsystems[i1].charmm_ff.atomtable.sigma[i]*10);  // convert to Angstroms
				
			}
		}
		else{
			for(auto ln : inp.get_child("sub1"))
			{
				if(ln.first == "<xmlattr>" || ln.first == "<xmlcomment>")
					continue;
				
				std::string tag = ln.first;
				
				mpe[tag]=ln.second.get<double>("<xmlattr>.epsilon",0);
				mps[tag]=ln.second.get<double>("<xmlattr>.sigma",0);
			}
			for(int i : s.s1tocfg)
			{
				s.ljepsilon_s1.push_back(mpe[tags[i]]*en_conv);
				s.ljsigma_s1.push_back(mps[tags[i]]*l_conv);
				
			}
		}
		
		
		mpe.clear();
		mps.clear();
		if(tp2 == "charmm")
		{
			
			for( unsigned int i=0;i<s.s2tocfg.size();i++)
			{
				double kjmoltoev = 0.010364305332435094;
				s.ljepsilon_s2.push_back(subsystems[i2].charmm_ff.atomtable.epsilon[i]*kjmoltoev);
				s.ljsigma_s2.push_back(subsystems[i2].charmm_ff.atomtable.sigma[i]*10);
				
			}
		}
		else{
			
			for(auto ln : inp.get_child("sub2"))
			{
				if(ln.first == "<xmlattr>" || ln.first == "<xmlcomment>")
					continue;
				
				std::string tag = ln.first;
				
				mpe[tag]=ln.second.get<double>("<xmlattr>.epsilon",0);
				mps[tag]=ln.second.get<double>("<xmlattr>.sigma",0);
			}
			for(int i : s.s2tocfg)
			{
				s.ljepsilon_s2.push_back(mpe[tags[i]]*en_conv);
				s.ljsigma_s2.push_back(mps[tags[i]]*l_conv);
				
			}
		}
		
		
				
	
		intersub.push_back(s);
	}
	
	




	double ossEnergy(Subs& s, const arma::mat& cfg)
	{
		int knt=0;
		for(int ic : s.nO_to_cfg)
		{
			s.oss_nO(knt,0) = cfg(ic,0);
			s.oss_nO(knt,1) = cfg(ic,1);
			s.oss_nO(knt,2) = cfg(ic,2);
			knt++;
		}
		
		knt=0;
		for(int ic : s.nH_to_cfg)
		{
			s.oss_nH(knt,0) = cfg(ic,0);
			s.oss_nH(knt,1) = cfg(ic,1);
			s.oss_nH(knt,2) = cfg(ic,2);
			knt++;
		}
		
		double hatoev = 27.2114;
		double en = s.p_oss.feedCoordinates(s.oss_nO,s.oss_nH)*hatoev;
		if(std::isnan(en))
			std::cout<<"OSS NAN"<<std::endl;
				
		return en;
		
		
		
	}
	double charmmEnergy(Subs& s, const arma::mat& cfg)
	{
		// remember: charmff is in nanometers, this plugin uses angstroms
		int knt=0;
		for(int ic : s.charmm_to_cfg)
		{
			s.charmm_buff[knt][0]=cfg(ic,0)*0.1;
			s.charmm_buff[knt][1]=cfg(ic,1)*0.1;
			s.charmm_buff[knt][2]=cfg(ic,2)*0.1;
			knt++;
		}
		
		s.charmm_ff.feedCoordinates(s.charmm_buff);
		double en = s.charmm_ff.potentialEnergy();
		if(std::isnan(en))
			std::cout<<"CHARMM NAN"<<std::endl;
			 
		return en;
		//ff.gradient(coordinate_array,charmm_gradient);
		
	}
	double intersubEnergy(ISubs& s, const arma::mat& cfg)
	{
		// USING ONLY LORENTZ-BERTHELOT RULES FOR COMBINING LJ.
		// COULD BE EXTENDED IN THE FUTURE
		
		double en=0;
		
		for(unsigned int i = 0; i< s.s1tocfg.size();i++)
			for(unsigned int j = 0; j< s.s2tocfg.size();j++)
			{
				double epslj = sqrt(s.ljepsilon_s1[i] * s.ljepsilon_s2[j]);
				double siglj = 0.5*(s.ljsigma_s1[i] + s.ljsigma_s2[j]);
				
				double dquad = 0;
				for(int d=0;d<dimensions;d++)
					dquad += pow(cfg(s.s1tocfg[i],d)-cfg(s.s2tocfg[j],d),2);
		
				double dist = sqrt(dquad);
				double fac = pow(siglj/dist,6);
				double vlj = 4*epslj*fac*(fac-1);
				
				// coulomb part
				double q1 = charges[s.s1tocfg[i]];
				double q2 = charges[s.s2tocfg[j]];
				double vcoul = q1*q2/(dist*coulfac);
				
				en += vlj+vcoul;
			}
		
		if(std::isnan(en))
			std::cout<<"INTERSUB NAN"<<std::endl;
		
		
		return en;
		
	}
	double restEnergy(Rest& r, const arma::mat& cfg)
	{
		double dquad = 0;
		for(int d=0;d<dimensions;d++)
			dquad += pow(cfg(r.i1,d)-cfg(r.i2,d),2);

		double dist = sqrt(dquad);
		
		double en = r.k*pow(dist - r.d, r.nexp);
		
		return en;
	}
	
	double refRestEnergy(RefRest& r, const arma::mat& cfg)
	{
		double dquad = 0;
		for(int d=0;d<dimensions;d++)
			dquad += pow(cfg(r.idx,d)-r.ref(d),2);

		double dist = sqrt(dquad);
		
		double en = r.k*pow(dist, r.nexp);
		
		return en;
	}

	arma::mat ossForces(Subs& s, const arma::mat& cfg)
	{
		arma::mat f(N,dimensions,arma::fill::zeros);
		
		int knt=0;
		for(int ic : s.nO_to_cfg)
		{
			s.oss_nO(knt,0) = cfg(ic,0);
			s.oss_nO(knt,1) = cfg(ic,1);
			s.oss_nO(knt,2) = cfg(ic,2);
			knt++;
		}
		
		knt=0;
		for(int ic : s.nH_to_cfg)
		{
			s.oss_nH(knt,0) = cfg(ic,0);
			s.oss_nH(knt,1) = cfg(ic,1);
			s.oss_nH(knt,2) = cfg(ic,2);
			knt++;
		}
		
		s.p_oss.feedCoordinates(s.oss_nO,s.oss_nH);
		s.p_oss.computeForces();
		
		knt=0;
		double hatoev = 27.2114;
		for(int ic : s.nO_to_cfg)
			f.row(ic) = s.p_oss.FO.row(knt++)*hatoev;
		
		knt=0;
		for(int ic : s.nH_to_cfg)
			f.row(ic) = s.p_oss.FH.row(knt++)*hatoev;
		
		return f;
	
	}
	
	arma::mat charmmForces(Subs& s, const arma::mat& cfg)
	{
		arma::mat f(N,dimensions,arma::fill::zeros);
		
		// remember: charmff is in nanometers, this plugin uses angstroms
		int knt=0;
		for(int ic : s.charmm_to_cfg)
		{
			s.charmm_buff[knt][0]=cfg(ic,0)*0.1;
			s.charmm_buff[knt][1]=cfg(ic,1)*0.1;
			s.charmm_buff[knt][2]=cfg(ic,2)*0.1;
			knt++;
		}
		
		s.charmm_ff.feedCoordinates(s.charmm_buff);
		s.charmm_ff.gradient(s.charmm_gradients);
		
		knt=0;
		for(int ic : s.charmm_to_cfg)
		{
			f(ic,0)= -s.charmm_gradients[knt][0];
			f(ic,1)= -s.charmm_gradients[knt][1];
			f(ic,2)= -s.charmm_gradients[knt][2];
			knt++;
		}
		
		return f;
	
	}
	
	arma::mat intersubForces(ISubs& s, const arma::mat& cfg)
	{
		
		arma::mat f(N,dimensions,arma::fill::zeros);
		
		for(unsigned int i = 0; i< s.s1tocfg.size();i++)
			for(unsigned int j = 0; j< s.s2tocfg.size();j++)
			{
				double epslj = sqrt(s.ljepsilon_s1[i] * s.ljepsilon_s2[j]);
				double siglj = 0.5*(s.ljsigma_s1[i] + s.ljsigma_s2[j]);
				
				
				double dquad = 0;
				for(int d=0;d<dimensions;d++)
					dquad += pow(cfg(s.s1tocfg[i],d)-cfg(s.s2tocfg[j],d),2);
				
				double dist = sqrt(dquad);
				double fac = pow(siglj/dist,6);
				
				double drlj = -24*epslj*fac*(2*fac-1)/dist;
				
				// coulomb part
				double q1 = charges[s.s1tocfg[i]];
				double q2 = charges[s.s2tocfg[j]];
				
				double drcoul = -q1*q2/(dist*dist*coulfac);
				
				for(int d=0;d<dimensions;d++)
				{
					f(s.s1tocfg[i],d) -= (drlj+drcoul)*(cfg(s.s1tocfg[i],d)-cfg(s.s2tocfg[j],d))/dist; 
					f(s.s2tocfg[j],d) += (drlj+drcoul)*(cfg(s.s1tocfg[i],d)-cfg(s.s2tocfg[j],d))/dist;
				}
				
				
			}
		
		return f;
	
		
	}
	
	
	arma::mat restForces(Rest& r, const arma::mat& cfg)
	{
		arma::mat f(N,dimensions,arma::fill::zeros);
		
		double dquad = 0;
		for(int d=0;d<dimensions;d++)
			dquad += pow(cfg(r.i1,d)-cfg(r.i2,d),2);
		
		double dist = sqrt(dquad);
		double cfac = r.k*r.nexp*pow(dist - r.d, r.nexp-1);
		
		for(int d=0;d<dimensions;d++)
		{
			double dp = (cfg(r.i1,d) - cfg(r.i2,d))/dist;
			f(r.i1,d) = -cfac * dp;
			f(r.i2,d) = cfac * dp;
		}
		
		return f;
	}
	
	arma::mat refRestForces(RefRest& r, const arma::mat& cfg)
	{
		arma::mat f(N,dimensions,arma::fill::zeros);
		
		double dquad = 0;
		for(int d=0;d<dimensions;d++)
			dquad += pow(cfg(r.idx,d)-r.ref(d),2);
		
		double dist = sqrt(dquad);
		double cfac = r.k*r.nexp*pow(dist, r.nexp-1);
		
		for(int d=0;d<dimensions;d++)
		{
			double dp=0;
			if(fabs(dist)>1E-4)
				dp = (cfg(r.idx,d) - r.ref(d))/dist;
			
			f(r.idx,d) = -cfac * dp;
		}
		return f;
	}

	
	arma::mat MCSample()
	{
		arma::mat sconf(N,2*dimensions,arma::fill::zeros);
		
		return sconf;
	}
	
	arma::mat MDSample(int nsteps)
	{
		//arma::mat sconf(N,2*dimensions,arma::fill::zeros);
		
		
		arma::mat forces = ForcesMD(current_config);
			
		
		for(int i=0;i<nsteps;i++)
		{
			//langevin(current_config,0.5*md_dt);
			for(int j=0;j<N;j++)
				for(int d=0;d<dimensions;d++)
					current_config(j,d+dimensions) += 0.5*md_dt*econv_to_md*forces(j,d);
		
			
			for(auto vsite : vsites)
			{
				arma::rowvec v12(3);
				
				for(int d=0;d<3;d++)
					v12(d)= current_config(vsite.i2,d)-current_config(vsite.i1,d);
				
				double d12 = arma::dot(v12,v12);
				d12 = sqrt(d12);
				double doh = vsite.a * d12 + vsite.b;
				arma::rowvec ver = v12/d12;
				
				for(int d=0;d<3;d++)
					current_config(vsite.isite,d) = current_config(vsite.i1,d) + doh*ver(d);
				
				
			}
			
		
			for(int j=0;j<N;j++)
				for(int d=0;d<dimensions;d++)
					current_config(j,d) += md_dt*current_config(j,d+dimensions)/masses[j];
		
		
			forces = ForcesMD(current_config);
			//langevin(current_config,0.5*md_dt);
			for(int j=0;j<N;j++)
				for(int d=0;d<dimensions;d++)
					current_config(j,d+dimensions) += 0.5*md_dt*econv_to_md*forces(j,d);
					
			vrescale(current_config);
		
		}	
		
		return current_config;	
		
	}
	
	double kineticEnergy(arma::mat& cfg)
	{
		double ek=0;
		for(int i=0;i<N;i++)
			for(int d=0;d<dimensions;d++)
				ek += cfg(i,dimensions+d)*cfg(i,dimensions+d) / (2 * masses[i]);
		
		
		return ek/econv_to_md;
	}
	
	void vrescale(arma::mat& cfg)
	{
		double ndef = (N- freeze_list.size())*dimensions;
		double k = kineticEnergy(cfg);
		double ktarg = ndef/(2*beta);
		double kfac = ktarg/(ndef*k);
		
		double fex =exp(-md_dt/md_gammath);
		double g1 = ndist(rgen);
		double randpart = g1*g1;
		for(int i=1;i<ndef;i++)
			randpart += pow(ndist(rgen),2);
		
		double rsq = fex + kfac*(1-fex)*randpart;
		rsq += 2*exp(-0.5*md_dt/md_gammath)*sqrt(kfac*(1.0- fex))*g1;
		
		double r = sqrt(rsq);
		if(r>1.25 || r<0.8)
			r = 1;
			
		for(int i=0;i<N;i++)
			for(int d=0;d<dimensions;d++)
				cfg(i,dimensions+d) *= r;
	}
	
	void langevin(arma::mat& cfg, double dt)
	{
		return;
		for(int i=0;i<N;i++)
			for(int d=0;d<dimensions;d++)
			{
				cfg(i,d+dimensions) += -dt*md_gammath*cfg(i,d+dimensions);
				cfg(i,d+dimensions) += sqrt(2*dt*md_gammath*masses[i]*econv_to_md/beta)*ndist(rgen);
			}
		
		for(int i : freeze_list)
			for(int d=0;d<dimensions;d++)
				cfg(i,d+dimensions)=0;
	
	}

	void gogogo()
	{
		std::cout<<"debug function gogogo invoked"<<std::endl;
		
		tmpo.open("tmp.xyz");
		tmpe.open("energy.dat");
		
		tmpo<<N<<std::endl<<std::endl;
		for(int i=0;i<N;i++)
			tmpo<<tags[i]<<"  "<<current_config(i,0)<<" "<<current_config(i,1)<<"  "<<current_config(i,2)<<std::endl;
		
		double ek = kineticEnergy(current_config);
		double ep = totalEnergy(current_config);
		arma::mat cf = Forces(current_config);
		
		tmpe<<ek<<" "<<ep<<" "<<ek+ep<<"  "<<atomEnergy(current_config,503)<<std::endl;
		
		double ffconv = (1.6021766208e-18/1.66053904e-27)*1E-6;
		//double kmoltoev=0.010364305332435094;
		/*double h = 0.001;
		for(int i=0;i<N;i++)
		{
			double fq=0;
			for(int d=0;d<3;d++)
			{
				arma::mat cfn1 = current_config;
				arma::mat cfn2 = current_config;
				cfn1(i,d) += h;
				cfn2(i,d) -= h;
				
				double E1 = totalEnergy(cfn1);
				double E2 = totalEnergy(cfn2);
				double numder = (E1-E2)/(2*h);
				double der = -cf(i,d);
				//if(fabs(der-numder)>1E-2)
				//	std::cout<<i<<"  "<<d<<"      "<<numder<<"   "<<der<<std::endl;
				
				
				fq += cf(i,d)*cf(i,d);
			}
			fq = sqrt(fq);
			std::cout<<i<<"  "<<fq<<std::endl;
			//std::cout<<i+1<<"   "<<current_config(i,0)<<"  "<<current_config(i,1)<<"  "<<current_config(i,2)<<"  "<<fq*ffconv<<std::endl;
		}
		
		
		
		throw;*/
		int NST=1000;
		int nbl=10;
		
		arma::mat bavg(N,6,arma::fill::zeros);
		arma::mat bavgq(N,6,arma::fill::zeros);
		
		
		arma::mat qbavg(N,6,arma::fill::zeros);
		arma::mat qbavgq(N,6,arma::fill::zeros);
		
		
		for(int b=0;b<nbl;b++)
		{
			arma::mat avg(N,6,arma::fill::zeros);
			arma::mat qavg(N,6,arma::fill::zeros);
			
			std::cout<<"Block "<<b+1<<" of "<<nbl<<std::endl;
			for(int ii=0;ii<NST;ii++)
			{
				MDSample();
				
				
				//arma::mat ff = Forces(current_config);
				//ff.print();
				//std::cout<<std::endl<<std::endl<<std::endl;
						
				
				tmpo<<N<<std::endl<<std::endl;
				for(int i=0;i<N;i++)
				{
					tmpo<<tags[i]<<"  "<<current_config(i,0)<<" "<<current_config(i,1)<<"  "<<current_config(i,2)<<std::endl;
					for(int d=0;d<6;d++)
					{
						avg(i,d) += current_config(i,d);
						qavg(i,d) += current_config(i,d)*current_config(i,d);
					}
				}
				ek = kineticEnergy(current_config);
				ep = totalEnergy(current_config);
				
				//tmpe<<ek<<" "<<ep<<" "<<ek+ep<<"  "<<atomEnergy(current_config,504)<<std::endl;
				tmpe<<ek<<" "<<ep<<" "<<ek+ep<<std::endl;
			
				
			}
			
			avg = avg / NST;
			bavg += avg;
			bavgq += avg % avg;
			
			qavg = qavg / NST;
			qbavg += qavg;
			qbavgq += qavg % qavg;
			
			
		}
		
		bavg = bavg / nbl;
		bavgq = bavgq / nbl;
		
		qbavg = qbavg / nbl;
		qbavgq = qbavgq / nbl;
		
		std::ofstream outx1("xs.dat");
		std::ofstream outp1("ps.dat");
		
		std::ofstream outxq("xsq.dat");
		std::ofstream outpq("psq.dat");
		
		double xavg=0;
		double pavg=0;
		
		double qxavg=0;
		double qpavg=0;
		
		for(int i=0;i<N;i++)
		{
			for(int d=0;d<6;d++)
			{
				double err = sqrt((bavgq(i,d)-bavg(i,d)*bavg(i,d))/(nbl-1));
				std::cout<<i<<"  "<<d<<"  "<<bavg(i,d)<<"  "<<err;
				if(d<3)
					outx1<<bavg(i,d)<<"  "<<err<<"   \t   ";
				else
					outp1<<bavg(i,d)<<"  "<<err<<"   \t   ";
				
				
				err = sqrt((qbavgq(i,d)-qbavg(i,d)*qbavg(i,d))/(nbl-1));
				std::cout<<"    quad  "<<d<<"  "<<qbavg(i,d)<<"  "<<err<<std::endl;
				if(d<3)
					outxq<<qbavg(i,d)<<"  "<<err<<"   \t   ";
				else
					outpq<<qbavg(i,d)<<"  "<<err<<"   \t   ";
				
					
				if(d<3)
				{
					xavg += bavg(i,d);
					qxavg += qbavg(i,d);
				}
				else{
					pavg += bavg(i,d);
					qpavg += qbavg(i,d);
				}
					
			}
			outx1<<std::endl;
			outxq<<std::endl;
			outp1<<std::endl;
			outpq<<std::endl;
			
		}
		
		xavg /= 3*N;
		pavg /= 3*N;
		qxavg /= 3*N;
		qpavg /= 3*N;
		
		
		std::cout<<"X AVG ="<<xavg<<std::endl;
		std::cout<<"P AVG ="<<pavg<<std::endl;
		std::cout<<"X AVG QUAD ="<<qxavg<<std::endl;
		std::cout<<"P AVG QUAD="<<qpavg<<std::endl;
		
		
		std::cout<<std::endl<<std::endl;	
		throw;
	}
	
	
	void equilibrate()
	{
		if(md_equil==0)
			return;
		
		if(mpirank==0)
			std::cout<<"** Equilibrating initial density... "<<std::endl;
		
		MDSample(md_equil);
		
		if(mpirank==0)
			std::cout<<" \t\t ... done."<<std::endl;
		
	}
}


