#include<iostream>
#include<armadillo>
#include<dlfcn.h>
#include<map>
#include<mpi.h>

void incrementMomenta(arma::mat&, arma::mat&, double);
void incrementPositions(arma::mat&,double);

void reduce(arma::mat&,int,int);
void outxyz(std::ofstream& outp, arma::mat& cfg);


void propagate(arma::mat&, int, double);


double ekin(arma::mat&);

int Nsol;
arma::mat masses;
double conv_force_to_md;




typedef const arma::vec (*getMasses_t)();
getMasses_t getMasses;
	
typedef arma::mat (*Boltzmann_t)();
Boltzmann_t Boltzmann;
	
typedef arma::mat (*Forces_t)(const arma::mat&);
Forces_t Forces;
			
	
typedef double (*Energy_atom)(const arma::mat&);
Energy_atom Energy;
	
	
typedef double (*Energy_single)(const arma::mat&, int);
Energy_single atomEnergy;

arma::mat forces;	
int main()
{
	
	int MCS = 10;
	int nblocks=5;
	
	double dt=5E-4;
	double dt_fdc=5E-4;
	int nsteps = 10000;
	int fdcafter = 0;
	int Nsub = 503;
	
	
	int nprop = dt_fdc/dt;
	dt = dt_fdc/nprop;
	std::cout<<"dt integration = "<<dt<<std::endl;
	std::cout<<"Total trajectory steps: "<<nsteps*nprop<<std::endl;
	
	MPI_Init(NULL,NULL);
	int mpirank,nranks;
	MPI_Comm_rank(MPI_COMM_WORLD,&mpirank);
	MPI_Comm_size(MPI_COMM_WORLD,&nranks);
	
	std::string solvent_inputfile = "mcinput.xml";
	arma::mat levels;
	levels<<-2.06743 << -0.492622 <<  -0.503749<<0<<0<<0<<arma::endr<<
	        -1.790896 << -0.452965 << -0.4157876<<0<<0<<0;
	
	
		
	
	
	
	
	
	std::string pgdir = "./mc_drive.so";
	void* handle = dlopen(pgdir.c_str(),RTLD_LAZY);
	
	if(!handle)
		throw std::runtime_error("PLUGIN LOADING WENT PEAR SHAPED");
		
		
	
	typedef void (*ldata_t)(int,const std::string&,bool);
	ldata_t load = (ldata_t)dlsym(handle,"loadData");
	bool verbose=false;
	if(mpirank==0)
		verbose=true;
		
	load(mpirank,solvent_inputfile,verbose);
	typedef int (*getN_t)();
	getN_t getN = (getN_t)dlsym(handle,"getSolventN");
	
	Nsol = getN();
	
	
	typedef std::map<std::string,double> (*getUnits_t)();
	getUnits_t getUnits = (getUnits_t)dlsym(handle,"getUnits");
	
	
	getMasses = (getMasses_t)dlsym(handle,"getMasses");
	Boltzmann = (Boltzmann_t)dlsym(handle,"Sample");
	Energy = (Energy_atom)dlsym(handle,"totalEnergy");
	Forces = (Forces_t)dlsym(handle,"Forces");
	atomEnergy = (Energy_single)dlsym(handle,"atomEnergy");
	
	masses = getMasses();
	std::map<std::string,double> solventUnits = getUnits();
	
	conv_force_to_md = solventUnits["energy"]*pow(solventUnits["time"]/solventUnits["length"],2)/solventUnits["mass"];
	
	arma::vec bedt(nsteps,arma::fill::zeros);
	arma::vec bedtq(nsteps,arma::fill::zeros);
	
	arma::vec bacdt(nsteps,arma::fill::zeros);
	arma::vec bacdtq(nsteps,arma::fill::zeros);
	
        arma::vec ntbacdt(nsteps,arma::fill::zeros);
        arma::vec ntbacdtq(nsteps,arma::fill::zeros);



	//std::ofstream outpt("traj.xyz");
	//std::ofstream oen("energies.dat");
	for(int i=0;i<nblocks;i++)
	{
		arma::vec edt(nsteps,arma::fill::zeros);
		arma::vec acdt(nsteps,arma::fill::zeros);
		arma::vec ntacdt(nsteps,arma::fill::zeros);
		if(mpirank==0){
			std::cout<<"Block "<<i+1<<" of "<<nblocks<<": [";
			std::cout.flush();
		}
		
		int ndisc = MCS/10;
		if(ndisc==0)
			ndisc=1;
			
		MPI_Barrier(MPI_COMM_WORLD);
		
		for(int j=0;j<MCS;j++)
		{
			if((j+1)%(ndisc) == 0 && mpirank==0)
			{
				std::cout<<"*";
				std::cout.flush();
			}
			
			arma::mat cfg = Boltzmann();
			
			//double ek,ep;
			//ek = ekin(cfg);
			//ep = Energy(cfg);
			//oen<<ek<<" "<<ep<<" "<<ek+ep<<std::endl;
			arma::vec evals(nsteps,arma::fill::zeros);
			double eavg=0;	
			for(int k=0;k<nsteps;k++)
			{
				double estate = Energy(cfg);
				cfg.row(Nsub) = levels.row(1);
				double eother = Energy(cfg);
				cfg.row(Nsub) = levels.row(0);
				double reorg = eother - estate;
								
				evals(k)=reorg;
				edt(k) += reorg;
				
				//ek = ekin(cfg);
				//ep = Energy(cfg);
				//oen<<ek<<" "<<ep<<" "<<ek+ep<<std::endl;
				
				if(k>=fdcafter)
					eavg += reorg;
				
				propagate(cfg,nprop,dt);
			}
			eavg /= (nsteps-fdcafter);
			arma::vec acdc(nsteps,arma::fill::zeros);
			
			for(int l=0;l<nsteps-fdcafter;l++)
			{
				for(int m=fdcafter;m<nsteps-l;m++)
					acdc(l) += (evals(m)-eavg)*(evals(m+l)-eavg);
				
				acdc(l) /= (nsteps-l-fdcafter);
			}

			for(int l=0;l<nsteps;l++)
				ntacdt(l) += evals(0)*evals(l);
			
			acdt += acdc;
		}
		
		if(mpirank==0)
			std::cout<<"]."<<std::endl;
		
		
		edt = edt / MCS;
		acdt = acdt / MCS;
		ntacdt = ntacdt / MCS;

		std::string edpd="output/edt_b"+std::to_string(i)+"_n"+std::to_string(mpirank)+".dat";
		std::string adpd="output/acdt_b"+std::to_string(i)+"_n"+std::to_string(mpirank)+".dat";
		 std::string ntadpd="output/ntacdt_b"+std::to_string(i)+"_n"+std::to_string(mpirank)+".dat";		


		std::ofstream outpde(edpd);
		std::ofstream outpda(adpd);
		std::ofstream ntoutpda(ntadpd);		

		for(int i=0;i<nsteps-fdcafter;i++)
		{
			outpde<<i*dt_fdc<<" "<<edt(i)<<std::endl;
			outpda<<i*dt_fdc<<" "<<acdt(i)<<std::endl;
		}

		for(int i=0;i<nsteps;i++)
			ntoutpda<<i*dt_fdc<<" "<<ntacdt(i)<<std::endl;

		reduce(edt,mpirank,nranks);
		reduce(acdt,mpirank,nranks);

		if(mpirank==0)
		{
			std::string edpda="edt_b"+std::to_string(i)+".dat";
			std::string adpda="acdt_b"+std::to_string(i)+".dat";
			std::string ntadpda="ntacdt_b"+std::to_string(i)+".dat";
			
			std::ofstream outpdea(edpda);
			std::ofstream outpdaa(adpda);
			std::ofstream ntoutpdaa(ntadpda);

			for(int i=0;i<nsteps-fdcafter;i++)
			{
				outpdea<<i*dt_fdc<<" "<<edt(i)<<std::endl;
				outpdaa<<i*dt_fdc<<" "<<acdt(i)<<std::endl;
			}
			
			for(int i=0;i<nsteps;i++)
				ntoutpdaa<<i*dt_fdc<<" "<<ntacdt(i)<<std::endl;

		}
		
		
		bedt = bedt + edt;
		bedtq = bedtq + edt % edt;
		
		bacdt = bacdt + acdt;
		bacdtq = bacdtq + acdt % acdt;
		
		ntbacdt = ntbacdt + ntacdt;
                ntbacdtq = ntbacdtq + ntacdt % ntacdt;		
		
	}
	
	bedt = bedt / nblocks;
	bedtq = bedtq / nblocks;
	
	bacdt = bacdt / nblocks;
	bacdtq = bacdtq / nblocks;
	
        ntbacdt = ntbacdt / nblocks;
        ntbacdtq = ntbacdtq / nblocks;

	
	if(mpirank==0)
	{
		std::ofstream outp("edt.dat");
		std::ofstream outpa("acorr.dat");
		std::ofstream ntoutpa("ntacorr.dat");		

		for(int i=0;i<nsteps-fdcafter;i++){
			outp<<i*dt_fdc<<" "<<bedt(i)<<"  "<<sqrt((bedtq(i)-bedt(i)*bedt(i))/(nblocks-1))<<std::endl;
			outpa<<i*dt_fdc<<" "<<bacdt(i)<<"  "<<sqrt((bacdtq(i)-bacdt(i)*bacdt(i))/(nblocks-1))<<std::endl;
		}

		for(int i=0;i<nsteps;i++)
			 ntoutpa<<i*dt_fdc<<" "<<ntbacdt(i)<<"  "<<sqrt((ntbacdtq(i)-ntbacdt(i)*ntbacdt(i))/(nblocks-1))<<std::endl;

	}
	
	
	MPI_Finalize();
	return 0;
}



void incrementMomenta(arma::mat& cfg, arma::mat& forces, double dt)
{
	
	for(int i=0;i<Nsol;i++)
		for(int d=0;d<3;d++)
			cfg(i,3+d) += forces(i,d)*dt;
			
}
void incrementPositions(arma::mat& cfg,double dt)
{
	for(int i=0;i<Nsol;i++)
		for(int d=0;d<3;d++)
			cfg(i,d) += cfg(i,3+d)*dt/masses(i);
			
}


void outxyz(std::ofstream &outp, arma::mat& cfg)
{
	outp<<Nsol<<std::endl<<std::endl;
	for(int i=0;i<Nsol;i++)
		outp<<"AT   "<<cfg(i,0)<<"  "<<cfg(i,1)<<"   "<<cfg(i,2)<<std::endl;
}



void reduce(arma::mat& data,int rank,int nranks)
{
	
	double* buffer = new double[data.n_elem];
	
	int knt=0;
	for(unsigned int i=0;i<data.n_rows;i++)
		for(unsigned int j=0;j<data.n_cols;j++)
			buffer[knt++]=data(i,j);
		
	
	if(rank!=0)
		MPI_Reduce(buffer,buffer,data.n_elem,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD); 
	else
		MPI_Reduce(MPI_IN_PLACE,buffer,data.n_elem,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD); 

	
	
	if(rank==0)
	{
		knt=0;
		for(unsigned int i=0;i<data.n_rows;i++)
			for(unsigned int j=0;j<data.n_cols;j++)
				data(i,j)=buffer[knt++]/nranks;
	}


	delete[] buffer;
}



void propagate(arma::mat& cfg, int nsteps, double dt)
{
	//arma::mat forces = conv_force_to_md*Forces(cfg);
	for(int k=0;k<nsteps;k++)
	{
		incrementPositions(cfg,dt);
		forces = conv_force_to_md*Forces(cfg);
		incrementMomenta(cfg,forces,dt);
	}
	
}



double ekin(arma::mat& cfg)
{
	double ek=0;
	for(int i=0;i<Nsol;i++)
	{
		double pq=0;
		for(int d=0;d<3;d++)
			pq += cfg(i,3+d)*cfg(i,3+d);
		
		ek += 0.5*pq/masses[i];
	}
	
	return ek/conv_force_to_md;
}


/*arma::mat forces = conv_force_to_md*Forces(cfg);
			incrementMomenta(cfg,forces,dt/2);
			
			arma::vec evals(nsteps,arma::fill::zeros);
			
			for(int k=0;k<nsteps-1;k++)
			{
				//outxyz(outpt,cfg);
				//arma::mat altcfg = cfg;
				//altcfg.row(Nsub) = levels.row(1);
			
				double estate = atomEnergy(cfg,Nsub);
				cfg.row(Nsub) = levels.row(1);
				double eother = atomEnergy(cfg,Nsub);
				cfg.row(Nsub) = levels.row(0);
				double reorg = eother - estate;
								
				evals(k)=reorg;
				edt(k) += reorg;
				
				
				
				incrementPositions(cfg,dt);
				
				forces = conv_force_to_md*Forces(cfg);
				incrementMomenta(cfg,forces,dt);
			}
			arma::mat altcfg = cfg;
			//outxyz(outpt,cfg);
			
			altcfg.row(Nsub) = levels.row(1);
			double estate = atomEnergy(cfg,Nsub);
			cfg.row(Nsub) = levels.row(1);
			double eother = atomEnergy(cfg,Nsub);
			cfg.row(Nsub) = levels.row(0);
			double reorg = eother - estate;
			edt(nsteps-1) += reorg;
			evals(nsteps-1) = reorg;
			*/
			
